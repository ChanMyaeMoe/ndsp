package com.example.ndsp.Holder;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.ndsp.Interface.OnClickRecyclerItemListener;
import com.example.ndsp.Pojo.Rabbit;
import com.example.ndsp.R;
import com.example.ndsp.model.Genre;

public class GenreListHolder extends RecyclerView.ViewHolder {

    private TextView name, id;
    private Context context;
    private LinearLayout layout;
    public static final String LANGUAGE_PREFERENCE = "lan_pref", PREFERENCE_KEY = "lan_key";
    private SharedPreferences sharedPreferences;


    OnClickRecyclerItemListener listener;


    public GenreListHolder(@NonNull View itemView, OnClickRecyclerItemListener listener) {
        super(itemView);
        this.listener = listener;
        this.context=itemView.getContext();
        sharedPreferences =context.getSharedPreferences(LANGUAGE_PREFERENCE, Context.MODE_PRIVATE);
        initHolder(itemView);

    }

    public void initHolder(View itemView) {

        layout = itemView.findViewById(R.id.layout);
        name = itemView.findViewById(R.id.tv_name);
        id = itemView.findViewById(R.id.tv_id);

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(Integer.parseInt(id.getText().toString()));
            }
        });
    }

    public void bindData(Genre item) {
        if (sharedPreferences.getString(PREFERENCE_KEY,"").equals("z")){

            name.setText(Rabbit.uni2zg(item.genrename));
            id.setText(Rabbit.uni2zg(String.valueOf(item.id)));

        }else if (sharedPreferences.getString(PREFERENCE_KEY,"").equals("u")){

            name.setText(Rabbit.zg2uni(item.genrename));
            id.setText(Rabbit.zg2uni(String.valueOf(item.id)));
        }else {

            name.setText(item.genrename);
            id.setText(String.valueOf(item.id));
        }

    }

    public static GenreListHolder create(LayoutInflater inflater, ViewGroup viewGroup, OnClickRecyclerItemListener listener) {
        View view = inflater.inflate(R.layout.list_view_item, viewGroup, false);

        return new GenreListHolder(view, listener);
    }
}
