package com.example.ndsp.Holder;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ndsp.ApiInterface.Api;
import com.example.ndsp.Interface.OnClickRecyclerItemListener;
import com.example.ndsp.Pojo.BookByCategory;
import com.example.ndsp.Pojo.Rabbit;
import com.example.ndsp.R;
import com.example.ndsp.RetrofitService.RetrofitService;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EbookListHolder extends RecyclerView.ViewHolder {

    private OnClickRecyclerItemListener listener;
    private TextView title,price,name,id,ebookDownloadUrl;
    private Context context;
    private ImageView imageView,buttonOptionMenu;
    LinearLayout layout;
    SharedPreferences sharedPreferences;
    public static final String LANGUAGE_PREFERENCE = "lan_pref", PREFERENCE_KEY = "lan_key";
    private RetrofitService service=new RetrofitService();
    private Api api=service.getRetrofitService().create(Api.class);

    public EbookListHolder(@NonNull View itemView, OnClickRecyclerItemListener  listener, Context context) {
        super(itemView);
        this.context=context;
        this.listener=listener;
        initView(itemView);
    }

    private void initView(View itemView) {

        id=itemView.findViewById(R.id.bookId);
        ebookDownloadUrl=itemView.findViewById(R.id.ebookdownload);
        title = itemView.findViewById(R.id.tvtitle);
        price = itemView.findViewById(R.id.tvprice);
        buttonOptionMenu=itemView.findViewById(R.id.textViewOptions);
        name = itemView.findViewById(R.id.tvname);
        imageView= itemView.findViewById(R.id.profile);
        layout=itemView.findViewById(R.id.author_layout_list);
        sharedPreferences=context.getSharedPreferences(LANGUAGE_PREFERENCE,Context.MODE_PRIVATE);
    }

    public void bindData(final BookByCategory book){
        if (sharedPreferences.getString(PREFERENCE_KEY,"").equals("z")){

            title.setText(Rabbit.uni2zg(book.bookTitle));
            price.setText(Rabbit.uni2zg(String.valueOf(book.ebookSize)));
            name.setText(Rabbit.uni2zg(book.author));


        }else if (sharedPreferences.getString(PREFERENCE_KEY,"").equals("u")){

            title.setText(Rabbit.zg2uni(book.bookTitle));
            price.setText(Rabbit.zg2uni(String.valueOf(book.ebookSize)));
            name.setText(Rabbit.zg2uni(book.bookTitle));

        }else {

            title.setText(book.bookTitle);
            price.setText(String.valueOf(book.ebookSize));
            name.setText(book.author);

        }
        id.setText(String.valueOf(book.id));
        ebookDownloadUrl.setText(book.ebookDownload);

        Picasso.get()
                .load("http://128.199.217.182/api/image/book/" + book.bookCoverImgUrl)
                .resize(700,900)
                .centerCrop()
                .into(imageView);

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                listener.onItemClick(book.id);


            }
        });
        buttonOptionMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //creating a popup menu
                PopupMenu popup = new PopupMenu(view.getContext(), buttonOptionMenu);
                //inflating menu from xml resource
                popup.inflate(R.menu.ebook_popup_menu);
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu1:
                                api.getPDF(ebookDownloadUrl.getText().toString()).enqueue(new Callback<ResponseBody>() {
                                    @Override
                                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                        if(response.isSuccessful()){
                                            writeResponseBodyToDisk(response.body(),title.getText().toString());
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                                    }
                                });
                                break;
                            case R.id.menu2:
                                listener.onItemClick(Integer.parseInt(id.getText().toString()));
                                break;

                        }
                        return false;
                    }
                });
                popup.show();

            }
        });

    }

    public static EbookListHolder create(LayoutInflater inflater, ViewGroup viewGroup, OnClickRecyclerItemListener listener, Context context) {

        View view = inflater.inflate(R.layout.layout_author_list, viewGroup, false);
        return new EbookListHolder(view,listener,context);
    }

    private boolean writeResponseBodyToDisk(ResponseBody body , String pdfName) {
        try {
            // todo change the file location/name according to your needs
            File futureStudioIconFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),pdfName+".pdf");

            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];

                long fileSize = body.contentLength();
                Log.e("body.contentLength",String.valueOf(fileSize));
                long fileSizeDownloaded = 0;

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(futureStudioIconFile);

                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);

                    fileSizeDownloaded += read;

//                    progressBar.setProgress(read);
                    Log.e("readProgress",String.valueOf(read));

                    Toast.makeText(context, "Downloading...", Toast.LENGTH_LONG).show();
                }

                Toast.makeText(context, "Downloaded", Toast.LENGTH_SHORT).show();

                outputStream.flush();

                return true;
            } catch (IOException e) {
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return false;
        }
    }

}

