package com.example.ndsp.Holder;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.ndsp.Pojo.Rabbit;
import com.example.ndsp.Pojo.BookResponse;
import com.example.ndsp.R;
import com.squareup.picasso.Picasso;

import static com.example.ndsp.RetrofitService.RetrofitService.BASE_URL;

public class RecentBooksRecyclerHolder extends RecyclerView.ViewHolder {

    private ImageView imageView;
    private TextView txtrecent;
    private LinearLayout layout;
    private Context context;
    public static final String LANGUAGE_PREFERENCE = "lan_pref", PREFERENCE_KEY = "lan_key";
    private SharedPreferences sharedPreferences;
//    Book_Id book_id=new Book_Id();


    public interface OnItemClicked {
        void onBookItemClick(int position);
    }

    OnItemClicked onItemClicked;

    public RecentBooksRecyclerHolder(@NonNull View itemView, OnItemClicked onItemClicked) {
        super(itemView);
        this.onItemClicked = onItemClicked;
        this.context = itemView.getContext();
        sharedPreferences = context.getSharedPreferences(LANGUAGE_PREFERENCE, Context.MODE_PRIVATE);
        initHolder(itemView);

    }

    public void initHolder(View itemView) {

        imageView = itemView.findViewById(R.id.recent_book_image);
        txtrecent = itemView.findViewById(R.id.recent_book_text);
        layout = itemView.findViewById(R.id.recent_book_layout);

    }

    public void bindData(final BookResponse bookResponse) {

        if (sharedPreferences.getString(PREFERENCE_KEY, "").equals("z")) {

            txtrecent.setText(Rabbit.uni2zg(bookResponse.bookTitle));

        } else if (sharedPreferences.getString(PREFERENCE_KEY, "").equals("u")) {

            txtrecent.setText(Rabbit.zg2uni(bookResponse.bookTitle));

        } else {
            txtrecent.setText(bookResponse.bookTitle);
        }


        //Picasso
        Picasso.get().load(BASE_URL + "/api/image/book/" + bookResponse.bookCoverImgUrl).into(imageView);

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Log.d("book_id", String.valueOf(bookResponse.id));
                onItemClicked.onBookItemClick(bookResponse.id);
            }
        });
    }

    public static RecentBooksRecyclerHolder create(LayoutInflater inflater, ViewGroup viewGroup, OnItemClicked onItemClicked) {
        View view = inflater.inflate(R.layout.recent_book_layout, viewGroup, false);

        return new RecentBooksRecyclerHolder(view, onItemClicked);
    }

}
