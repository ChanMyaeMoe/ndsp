package com.example.ndsp.Holder;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.ndsp.Interface.OnClickRecyclerItemListener;
import com.example.ndsp.Pojo.Rabbit;
import com.example.ndsp.Pojo.TenCategoryResponse;
import com.example.ndsp.R;

import java.util.ArrayList;
import java.util.Random;

public class CategoryRecyclerHolder extends RecyclerView.ViewHolder {

    private TextView textView, tvId;
    private LinearLayout layout;
    private int i;
    public static final String LANGUAGE_PREFERENCE = "lan_pref", PREFERENCE_KEY = "lan_key";
    private SharedPreferences sharedPreferences;
    private Context context;
    private OnClickRecyclerItemListener listener;

    public CategoryRecyclerHolder(@NonNull View itemView, OnClickRecyclerItemListener listener) {
        super(itemView);
        this.context = itemView.getContext();
        sharedPreferences = context.getSharedPreferences(LANGUAGE_PREFERENCE, Context.MODE_PRIVATE);
        this.listener = listener;
        initView(itemView);

    }

    public void initView(View itemview) {
        tvId = itemview.findViewById(R.id.cate_id);
        textView = itemview.findViewById(R.id.txt_category_list);
        layout = itemview.findViewById(R.id.categories_recycler_layout);

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void bindData(final TenCategoryResponse category) {
        if (sharedPreferences.getString(PREFERENCE_KEY, "").equals("z")) {

            textView.setText(Rabbit.uni2zg(category.categoryName));

        } else if (sharedPreferences.getString(PREFERENCE_KEY, "").equals("u")) {

            textView.setText(Rabbit.zg2uni(category.categoryName));
        } else {

            textView.setText(category.categoryName);

        }

        tvId.setText(String.valueOf(category.id));

        Random rnd = new Random();
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setCornerRadius(10);
        int color = Color.argb(80, rnd.nextInt(200), rnd.nextInt(200), rnd.nextInt(200));

        if (i % 1 == 0) {
            gradientDrawable.setColor(color);
        } else {
            gradientDrawable.setColor(Color.BLUE);
        }
        textView.setBackground(gradientDrawable);

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                listener.onItemClick(Integer.parseInt(tvId.getText().toString()));

            }
        });

    }

    public static CategoryRecyclerHolder create(LayoutInflater inflater, ViewGroup viewGroup, OnClickRecyclerItemListener listener) {
        View view = inflater.inflate(R.layout.categories_recycler_layout, viewGroup, false);
        return new CategoryRecyclerHolder(view, listener);

    }
}
