package com.example.ndsp.Holder;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.ndsp.DataBase.OrderItemDatabaseManagement;
import com.example.ndsp.Interface.OnClickRecyclerItemListener;
import com.example.ndsp.Pojo.OrderBook;
import com.example.ndsp.Pojo.Rabbit;
import com.example.ndsp.R;

import java.util.Random;

public class OrderHolder extends RecyclerView.ViewHolder {

    private View color;
    private ImageView deleteIcon;
    private TextView title, price, qty, kyat;
    private ImageButton add, sub;
    private Context context;
    private OnClickRecyclerItemListener listener;
    private OrderItemDatabaseManagement database;
    private View viewColor;
    public static final String LANGUAGE_PREFERENCE = "lan_pref", PREFERENCE_KEY = "lan_key";
    private SharedPreferences sharedPreferences;


    public OrderHolder(@NonNull View itemView, OnClickRecyclerItemListener listener, Context context) {

        super(itemView);
        this.listener = listener;
        this.context = context;
        sharedPreferences = itemView.getContext().getSharedPreferences(LANGUAGE_PREFERENCE, Context.MODE_PRIVATE);
        initView(itemView);

    }

    private void initView(View itemView) {
        database = new OrderItemDatabaseManagement(context);
        kyat = itemView.findViewById(R.id.txtView_header_kyats);

        viewColor = itemView.findViewById(R.id.view_color);
        add = itemView.findViewById(R.id.imgBtn_add);
        sub = itemView.findViewById(R.id.imgBtn_substract);
        color = itemView.findViewById(R.id.view_color);
        deleteIcon = itemView.findViewById(R.id.edit_icon);
        title = itemView.findViewById(R.id.order_book_name);
        price = itemView.findViewById(R.id.order_book_price);
        qty = itemView.findViewById(R.id.order_book_qty);
    }

    public void bindData(final OrderBook orderBook) {

        Random rand = new Random();
        viewColor.setBackgroundColor(Color.rgb(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255)));

        final int[] book_qty = {orderBook.getBook_qty()};
        title.setText(orderBook.getBook_name());


        price.setText(String.valueOf(orderBook.getBook_price()));
        qty.setText(String.valueOf(book_qty[0]));

        sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (book_qty[0] > 1) {
                    book_qty[0]--;
                    database.dbOpen();
                    database.updateQuantity(String.valueOf(book_qty[0]), orderBook.getBook_id());
                    database.dbClose();

                    listener.onItemClick(Integer.parseInt(orderBook.getBook_id()));
                }
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                book_qty[0]++;
                qty.setText(String.valueOf(book_qty[0]));
                database.dbOpen();
                database.updateQuantity(String.valueOf(book_qty[0]), orderBook.getBook_id());
                database.dbClose();
                listener.onItemClick(Integer.parseInt(orderBook.getBook_id()));
            }
        });
        deleteIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                database.dbOpen();
                database.deleteOrderItem(orderBook.getBook_id());
                database.dbClose();
                listener.onItemClick(Integer.parseInt(orderBook.getBook_id()));

            }


        });


    }

    public static OrderHolder create(LayoutInflater inflater, ViewGroup viewGroup, OnClickRecyclerItemListener listener, Context context) {

        View view = inflater.inflate(R.layout.order_list_item, viewGroup, false);
        return new OrderHolder(view, listener, context);
    }


}
