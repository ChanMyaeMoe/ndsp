package com.example.ndsp.Holder;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.ndsp.Pojo.Rabbit;
import com.example.ndsp.R;
import com.example.ndsp.model.PublisherDetail;
import com.squareup.picasso.Picasso;

import static com.example.ndsp.RetrofitService.RetrofitService.BASE_URL;

public class Publisherholders extends RecyclerView.ViewHolder{

    private OnPublisherClickListener listener;
    private TextView title,price,name,tvid;
    private ImageView imageView,buttonoptionmenu;
    RatingBar bar;
    LinearLayout layout;
    private SharedPreferences sharedPreferences;
    public static final String LANGUAGE_PREFERENCE = "lan_pref", PREFERENCE_KEY = "lan_key";
    Context context;

    public Publisherholders(@NonNull View itemView, OnPublisherClickListener listener,Context context) {

        super(itemView);
        this.listener = listener;
        this.context=context;
        initView(itemView);
//        itemView.setOnClickListener();
    }



    private void initView(View itemView) {

        title = itemView.findViewById(R.id.tv_title);
        price = itemView.findViewById(R.id.tv_price);
        name = itemView.findViewById(R.id.tv_name);
        bar=itemView.findViewById(R.id.rating_Bar);
        imageView= itemView.findViewById(R.id.profile_list);
        buttonoptionmenu=itemView.findViewById(R.id.textView_Options);
        layout = itemView.findViewById(R.id.publisher_layout_list);
        sharedPreferences=context.getSharedPreferences(LANGUAGE_PREFERENCE, Context.MODE_PRIVATE);


    }

    public static Publisherholders create(LayoutInflater inflater, ViewGroup viewGroup, OnPublisherClickListener listener, Context context) {

        View view = inflater.inflate(R.layout.layout_publisher_list, viewGroup, false);
        return new Publisherholders(view,listener,context);
    }

    public void bindData(final PublisherDetail publisher) {
        Log.e("Publisher_ID",String.valueOf(publisher.id));
        if (sharedPreferences.getString(PREFERENCE_KEY,"").equals("z")){
            title.setText(Rabbit.uni2zg(publisher.booktitle));
            price.setText(Rabbit.uni2zg(String.valueOf(publisher.booksaleprice)));
            name.setText(Rabbit.uni2zg(publisher.author));
        }else if (sharedPreferences.getString(PREFERENCE_KEY,"").equals("u")){
            title.setText(Rabbit.zg2uni(publisher.booktitle));
            price.setText(Rabbit.zg2uni(String.valueOf(publisher.booksaleprice)));
            name.setText(Rabbit.zg2uni(publisher.author));
        }else {

            title.setText(publisher.booktitle);
            price.setText(String.valueOf(publisher.booksaleprice));
            name.setText(publisher.author);
        }


        Log.e("bookcovername",publisher.bookcover);

        Picasso.get().load("http://128.199.217.182/api/image/book/"+publisher.bookcover).into(imageView);

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onPublisherClick(publisher.id);
            }
        });
        buttonoptionmenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //creating a popup menu
                PopupMenu popup = new PopupMenu(view.getContext(), buttonoptionmenu);
                //inflating menu from xml resource
                popup.inflate(R.menu.option_popup_menu);
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu1:
                                return true;
                            case R.id.menu2:
                                listener.onPublisherClick(publisher.id);
                                return true;

                        }
                        return false;
                    }
                });
                popup.show();

            }
        });

    }

    //    @Override
//    public void onClick(View v) {
//
//        listener.onPublisherClick(Integer.parseInt(tvid.getText().toString()));
//        int position;
//        position = getAdapterPosition();
//        Log.e("position",String.valueOf(position));
//
//    }
    public interface  OnPublisherClickListener {
        void onPublisherClick(int id);
    }

}