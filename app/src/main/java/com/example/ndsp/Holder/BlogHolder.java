package com.example.ndsp.Holder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ndsp.Interface.OnClickRecyclerItemListener;
import com.example.ndsp.Pojo.Blog;
import com.example.ndsp.R;
import com.squareup.picasso.Picasso;

public class BlogHolder extends RecyclerView.ViewHolder {

    private OnClickRecyclerItemListener listener;
    private ImageView imageView;
    private TextView category, date, title, article, id;

    public BlogHolder(@NonNull View itemView, OnClickRecyclerItemListener listener) {
        super(itemView);
        this.listener = listener;
        initHolder(itemView);
    }

    private void initHolder(View itemView) {

        imageView = itemView.findViewById(R.id.img_blog);
        category = itemView.findViewById(R.id.blog_category);
        date = itemView.findViewById(R.id.date);
        title = itemView.findViewById(R.id.blog_title);
        article = itemView.findViewById(R.id.read_article);
        id = itemView.findViewById(R.id.id);

    }

    public void bindData(Blog blog) {

        Picasso.get()
                .load("http://128.199.217.182/api/image/book/" + blog.coverPhoto)
                .resize(300, 300)
                .centerCrop()
                .into(imageView);

        category.setText(blog.category);
        date.setText(blog.createAt);
        title.setText(blog.title);
        id.setText(blog.blogId);

        article.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(Integer.parseInt(id.getText().toString()));
            }
        });

    }

    public static BlogHolder create(LayoutInflater layoutInflater, ViewGroup viewGroup, OnClickRecyclerItemListener listener) {

        View view = layoutInflater.inflate(R.layout.layout_blog_list, viewGroup, false);
        return new BlogHolder(view, listener);

    }
}
