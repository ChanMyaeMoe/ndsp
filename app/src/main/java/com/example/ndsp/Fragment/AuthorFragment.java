package com.example.ndsp.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ndsp.Adapter.AuthorListAdapter;
import com.example.ndsp.ApiInterface.Api;
import com.example.ndsp.Interface.EndlessRecyclerOnScrollListener;
import com.example.ndsp.Interface.OnClickRecyclerItemListener;
import com.example.ndsp.Pojo.AuthorListResponse;
import com.example.ndsp.R;
import com.example.ndsp.RetrofitService.RetrofitService;
import com.example.ndsp.model.Author;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AuthorFragment extends Fragment implements OnClickRecyclerItemListener {
    private RecyclerView recyclerView;
    List<Author> authorcategories = new ArrayList<>();
    private RetrofitService service;
    private int currentPage = 1;
    private Api authorListApi;
    private AuthorListAdapter adapter;

    public AuthorFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_author, container, false);
        initAuthorList(view);
        return view;
    }


    private void getAuthorList(final int page) {

        authorListApi.getAuthorList(page).enqueue(new Callback<AuthorListResponse>() {
            @Override
            public void onResponse(Call<AuthorListResponse> call, Response<AuthorListResponse> response) {

                Log.e("currentPage", String.valueOf(page));

                if (response.isSuccessful()) {
                    adapter.addData(response.body().authors);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<AuthorListResponse> call, Throwable t) {
                Log.e("onfailure", t.toString());

            }
        });

    }


    private void initAuthorList(View view) {
        service = new RetrofitService();
        authorListApi = service.getRetrofitService().create(Api.class);
        recyclerView = view.findViewById(R.id.authorlist);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        adapter = new AuthorListAdapter(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String usrInput = bundle.getString("user_input");
            getSearchItem(usrInput);

        } else {

            recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(layoutManager) {
                @Override
                public void onLoadMore(int current_page) {

                    getAuthorList(current_page);

                }
            });
            getAuthorList(currentPage);
        }


    }

    private void getSearchItem(String usrInput) {

        authorListApi.getSearchAuthor(usrInput).enqueue(new Callback<List<Author>>() {
            @Override
            public void onResponse(Call<List<Author>> call, Response<List<Author>> response) {
                if(response.isSuccessful()){
                    adapter.addData(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Author>> call, Throwable t) {

            }
        });
    }

    @Override
    public void onItemClick(int position) {

        AuthorListFragment publisherListFragment = new AuthorListFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("author_id", position);
        publisherListFragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, publisherListFragment).commit();

    }
}
