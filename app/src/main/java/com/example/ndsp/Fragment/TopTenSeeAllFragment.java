package com.example.ndsp.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ndsp.Adapter.AuthorAdapter;
import com.example.ndsp.Adapter.CategoryListAdapter;
import com.example.ndsp.ApiInterface.Api;
import com.example.ndsp.Holder.AuthorHolder;
import com.example.ndsp.Interface.EndlessRecyclerOnScrollListener;
import com.example.ndsp.Pojo.BookResponse;
import com.example.ndsp.Pojo.OrderBook;
import com.example.ndsp.Pojo.RecentBookAllList;
import com.example.ndsp.Pojo.TopTenSeeAllBook;
import com.example.ndsp.R;
import com.example.ndsp.RetrofitService.RetrofitService;
import com.example.ndsp.model.AuthorDetail;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class TopTenSeeAllFragment extends Fragment implements AuthorHolder.OnAuthorClickListener{
    private RetrofitService retrofitService;
    private RecyclerView recyclerView;
    private AuthorAdapter adapter;
    private LinearLayoutManager layoutManager;
    private Api api;
    private int currentPage=1;


    public TopTenSeeAllFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_top_ten_see_all2, container, false);
        initResource(view);
        return view;
    }

    public void initResource(View view){

        retrofitService=new RetrofitService();
        api=retrofitService.getRetrofitService().create(Api.class);
        recyclerView=view.findViewById(R.id.recyclerView);
        layoutManager=new LinearLayoutManager(getContext());
        adapter=new AuthorAdapter(this,getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        getBookList(currentPage);

        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                getBookList(current_page);
            }
        });


    }

    public void getBookList(int currentPage){

        Log.e("currentPage",String.valueOf(currentPage));
        api.getTopTenSeeAll(currentPage).enqueue(new Callback<RecentBookAllList>() {
            @Override
            public void onResponse(Call<RecentBookAllList> call, Response<RecentBookAllList> response) {
                if(response.isSuccessful()){

                    adapter.addItem(response.body().getBookListData);
                }
            }

            @Override
            public void onFailure(Call<RecentBookAllList> call, Throwable t) {

                Log.e("error",t.toString());

            }
        });
    }

    @Override
    public void onAuthorClick(int id) {
        FragmentRecentBookDetail fragmentRecentBookDetail=new FragmentRecentBookDetail();
        Bundle bundle=new Bundle();
        bundle.putInt("book_id",id);
        fragmentRecentBookDetail.setArguments(bundle);
        FragmentTransaction fragmentTransaction=getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_container,fragmentRecentBookDetail).commit();

        Log.e("book_list_id",String.valueOf(id));

    }

    @Override
    public void addToDatabase(OrderBook orderBook) {

    }
}
