package com.example.ndsp.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ndsp.Adapter.AuthorAdapter;
import com.example.ndsp.ApiInterface.Api;
import com.example.ndsp.DataBase.OrderItemDatabaseManagement;
import com.example.ndsp.Holder.AuthorHolder;
import com.example.ndsp.Interface.CardDatabaseInterface;
import com.example.ndsp.Interface.EndlessRecyclerOnScrollListener;
import com.example.ndsp.Pojo.AuthorDetailResponse;
import com.example.ndsp.Pojo.OrderBook;
import com.example.ndsp.R;
import com.example.ndsp.RetrofitService.RetrofitService;
import com.example.ndsp.model.AuthorDetail;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AuthorListFragment extends Fragment implements AuthorHolder.OnAuthorClickListener {

    private RetrofitService service;
    private RecyclerView recyclerView;

    private int author_id;
    private AuthorAdapter adapter;
    private LinearLayoutManager layoutManager;
    private int currentPage=1;
    private  Api authorDetailApi;
    private OrderItemDatabaseManagement database;

    public AuthorListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_author_list, container, false);
        initActivity(view);
        return view;
    }

    private void initActivity(View view) {

        database=new OrderItemDatabaseManagement(getContext());
        service = new RetrofitService();
        authorDetailApi = service.getRetrofitService().create(Api.class);
        recyclerView =view.findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new AuthorAdapter(this,getContext());
        recyclerView.setAdapter(adapter);


        Bundle bundle =this.getArguments();
        if(bundle != null) {
            author_id = bundle.getInt("author_id");
            Log.e("author_id",String.valueOf(author_id));
        }

        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                getAuthorDetail(current_page);
            }
        });
        getAuthorDetail(currentPage);

    }
    private void getAuthorDetail(int currentPage) {
        Log.e("currentPage",String.valueOf(currentPage));

        authorDetailApi.getAuthorDetail(author_id,currentPage).enqueue(new Callback<AuthorDetailResponse>() {
            @Override
            public void onResponse(Call<AuthorDetailResponse> call, Response<AuthorDetailResponse> response) {

                if(response.isSuccessful()){

                    adapter.addItem(response.body().author);


                }
            }

            @Override
            public void onFailure(Call<AuthorDetailResponse> call, Throwable t) {

            }
        });

    }

    @Override
    public void onAuthorClick(int id) {
        FragmentRecentBookDetail fragmentRecentBookDetail=new FragmentRecentBookDetail();
        Bundle bundle=new Bundle();
        bundle.putInt("book_id",id);
//        bundle.putString("book_type", String.valueOf(categoryName));
        fragmentRecentBookDetail.setArguments(bundle);
        FragmentTransaction fragmentTransaction=getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_container,fragmentRecentBookDetail).commit();

        Log.e("book_list_id",String.valueOf(id));
    }

    @Override
    public void addToDatabase(OrderBook orderBook) {

        Log.e("bookTitle",orderBook.getBook_name());
        database.dbOpen();
        database.addOrderItem(orderBook);
        database.dbClose();
    }

}
