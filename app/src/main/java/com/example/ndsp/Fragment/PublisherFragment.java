package com.example.ndsp.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.ndsp.Adapter.PublisherListAdapter;
import com.example.ndsp.ApiInterface.Api;
import com.example.ndsp.Interface.EndlessRecyclerOnScrollListener;
import com.example.ndsp.Interface.OnClickRecyclerItemListener;
import com.example.ndsp.Pojo.PublisherListResponse;
import com.example.ndsp.R;
import com.example.ndsp.RetrofitService.RetrofitService;
import com.example.ndsp.model.Publisher;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PublisherFragment extends Fragment implements OnClickRecyclerItemListener {

    private RecyclerView recyclerView;
    private RetrofitService service;
    private PublisherListAdapter adapter;
    private int currentPage=1;
    private   Api getPublisherList;

    public PublisherFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_publisher, container, false);
        initPublisherList(view);
        return view;
    }

    private void initPublisherList(View view) {

        recyclerView = view.findViewById(R.id.publisherlist);
        service = new RetrofitService();
        getPublisherList = service.getRetrofitService().create(Api.class);
        adapter=new PublisherListAdapter(this);
        LinearLayoutManager layoutManager=new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String usrInput = bundle.getString("user_input");
            getSearchItem(usrInput);

        } else {

            getPublisherList(currentPage);
            recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(layoutManager) {
                @Override
                public void onLoadMore(int current_page) {
                    currentPage = current_page;
                    getPublisherList(current_page);
                }
            });
        }

    }

    private void getSearchItem(String usrInput) {
        getPublisherList.getSearchPublisher(usrInput).enqueue(new Callback<List<Publisher>>() {
            @Override
            public void onResponse(Call<List<Publisher>> call, Response<List<Publisher>> response) {
                if(response.isSuccessful()){
                    adapter.addData(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Publisher>> call, Throwable t) {

            }
        });
    }

    private void getPublisherList(int current_page) {
        getPublisherList.getPublisherList(current_page).enqueue(new Callback<PublisherListResponse>() {
            @Override
            public void onResponse(Call<PublisherListResponse> call, Response<PublisherListResponse> response) {

                if (response.isSuccessful()) {
                    Log.e("currentPage",String.valueOf(currentPage));
                        adapter.addData(response.body().publishers);

                }

            }

            @Override
            public void onFailure(Call<PublisherListResponse> call, Throwable t) {

                Log.e("onfailure", t.toString());


            }
        });

    }

    @Override
    public void onItemClick(int position) {
        PublisherListFragment publisherListFragment=new PublisherListFragment();
        Bundle bundle=new Bundle();
        bundle.putInt("publisher_id", position);
        publisherListFragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction=getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_container,publisherListFragment).commit();
    }
}
