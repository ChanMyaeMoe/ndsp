package com.example.ndsp.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ndsp.ApiInterface.Api;
import com.example.ndsp.Pojo.Blog;
import com.example.ndsp.R;
import com.example.ndsp.RetrofitService.RetrofitService;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class BlogDetailDFragment extends Fragment {

    private ImageView imageView;
    private TextView category, date, title, article, id;
    private RetrofitService service;
    private Api api;
    public BlogDetailDFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_blog_detail_d, container, false);
        initFragment(view);
        return view;
    }

    private void initFragment(View itemView) {
        service=new RetrofitService();
        api=service.getRetrofitService().create(Api.class);

        imageView = itemView.findViewById(R.id.img_blog);
        category = itemView.findViewById(R.id.blog_category);
        date = itemView.findViewById(R.id.date);
        title = itemView.findViewById(R.id.blog_title);
        article = itemView.findViewById(R.id.read_article);
        id = itemView.findViewById(R.id.id);

        getBlogDetail();
    }

    private void getBlogDetail(){

        api.getArticleDetail(2).enqueue(new Callback<Blog>() {
            @Override
            public void onResponse(Call<Blog> call, Response<Blog> response) {
                if(response.isSuccessful()){

                    Blog blog=response.body();
                    Picasso.get()
                            .load("http://128.199.217.182/api/image/blog/" + blog.coverPhoto)
                            .resize(300, 300)
                            .centerCrop();
                    category.setText(blog.category);
                    date.setText(blog.createAt);
                    title.setText(blog.title);
                    article.setText(blog.content);

                }
            }

            @Override
            public void onFailure(Call<Blog> call, Throwable t) {

            }
        });
    }

}
