package com.example.ndsp.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ndsp.Adapter.CategoryListAdapter;
import com.example.ndsp.ApiInterface.Api;
import com.example.ndsp.Interface.EndlessRecyclerOnScrollListener;
import com.example.ndsp.Interface.OnClickRecyclerItemListener;
import com.example.ndsp.Pojo.Category;
import com.example.ndsp.Pojo.CategoryAll;
import com.example.ndsp.R;
import com.example.ndsp.RetrofitService.RetrofitService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentCategory extends Fragment implements OnClickRecyclerItemListener {
    private RecyclerView recyclerView;
    private RetrofitService retrofitService;
    private Api api;
    private int currentPage=1;
    private LinearLayoutManager layoutManager;
    private CategoryListAdapter adapter;
    public FragmentCategory() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_fragment_category, container, false);
        initFragment(view);
        return view;
    }

    private void initFragment(View view) {
        retrofitService=new RetrofitService();
        api=retrofitService.getRetrofitService().create(Api.class);
        recyclerView=view.findViewById(R.id.recyclerView);
        layoutManager=new LinearLayoutManager(getContext());
        adapter=new CategoryListAdapter(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);



        Bundle bundle=this.getArguments();
        if(bundle!=null){
            String usrInput=bundle.getString("user_input");
            getSearchItem(usrInput);

        }
        else {
            getCategoryItmes(currentPage);
            recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(layoutManager) {
                @Override
                public void onLoadMore(int current_page) {
                    getCategoryItmes(current_page);
                }
            });
        }


    }

    private void getSearchItem(String usrInput) {
        Log.e("userInput",usrInput);
        api.getSearchCategory(usrInput).enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                if(response.isSuccessful()){
                    adapter.addItem(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {

            }
        });
    }

    public void getCategoryItmes(int currentPage){
        Log.e("currentPage;",String.valueOf(currentPage));
        api.getCategoryAll(currentPage).enqueue(new Callback<CategoryAll>() {
            @Override
            public void onResponse(Call<CategoryAll> call, final Response<CategoryAll> response) {

                if (response.isSuccessful()){

                    adapter.addItem(response.body().categoryAllDataResponseList);

                    }
                }
            @Override
            public void onFailure(Call<CategoryAll> call, Throwable t) {
                Log.e("CategoryAll",t.toString());
            }
        });
    }

    @Override
    public void onItemClick(int position) {

        CategoryItemFragment fragment=new CategoryItemFragment();
        Bundle bundle=new Bundle();
        bundle.putInt("category_id",position);
        fragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction=getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_container,fragment).commit();
    }
}