package com.example.ndsp.Fragment;

import android.Manifest;
import android.app.DownloadManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ndsp.Activity.MainActivity;
import com.example.ndsp.Adapter.CommentRecyclerAdapter;
import com.example.ndsp.ApiInterface.Api;
import com.example.ndsp.DataBase.OrderItemDatabaseManagement;
import com.example.ndsp.Pojo.Comment;
import com.example.ndsp.Pojo.CommentData;
import com.example.ndsp.Pojo.CommentResponse;
import com.example.ndsp.Pojo.OrderBook;
import com.example.ndsp.Pojo.Rabbit;
import com.example.ndsp.Pojo.RecentBookDetailResponce;
import com.example.ndsp.R;
import com.example.ndsp.RetrofitService.RetrofitService;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import io.github.yavski.fabspeeddial.FabSpeedDial;
import io.github.yavski.fabspeeddial.SimpleMenuListenerAdapter;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.support.constraint.Constraints.TAG;
import static com.example.ndsp.RetrofitService.RetrofitService.BASE_URL;

public class EbookDetailFragment extends Fragment {
    private RatingBar ratingBar;
    private TextView txtbooknameshow, txtauthornameshow, txtratingbarnumber, txtbookdetail, txtnameinfo, txtauthorinfo, txtpublisherinfo, txttypeinfo, txteditioninfo, txtsizeinfo;
    private Button btnseeallcomment;
    private RecyclerView recyclercomment;
    private TextView txtdetail, txtcontinue;
    private ImageView imageView;
    private RetrofitService retrofitService = new RetrofitService();
    int book_id;
    private FabSpeedDial fabSpeedDial;
    private OrderBook orderBook;
    int currentPage = 1;
    private CommentRecyclerAdapter adapter;
    private Api api;
    public static final String LANGUAGE_PREFERENCE = "lan_pref", PREFERENCE_KEY = "lan_key";
    private SharedPreferences sharedPreferences;
    private Context context;
    private String pdfLink,pdfName;
    public static String FRAGMENTBOOKDETAIL = FragmentRecentBookDetail.class.getSimpleName();
    private double totalFileSize;
    private ProgressBar progressBar;
    public EbookDetailFragment() {
        // Required empty public constructor

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        this.context=container.getContext();
        sharedPreferences =context.getSharedPreferences(LANGUAGE_PREFERENCE, Context.MODE_PRIVATE);

        return inflater.inflate(R.layout.fragment_ebook_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        initResoruce(view);
        super.onViewCreated(view, savedInstanceState);
    }




    private void fab_function() {
        fabSpeedDial.setMenuListener(new SimpleMenuListenerAdapter() {
            @Override
            public boolean onMenuItemSelected(MenuItem menuItem) {
                switch (menuItem.getItemId()) {

                    case R.id.book_detail_download:
                        Log.e("pdfName",pdfName);
                        api.getPDF(pdfLink).enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                if(response.isSuccessful()){
                                    Log.e("download",response.body().toString());
                                    writeResponseBodyToDisk(response.body());

                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {

                            }
                        });
                        break;
                    case R.id.book_detail_comment:
                        displayCommentDialog();
                        break;
                }
                return true;
            }
        });
    }


    public void initResoruce(View view) {
        api = retrofitService.getRetrofitService().create(Api.class);
        fabSpeedDial = getActivity().findViewById(R.id.fab_speedDial);
        retrofitService = new RetrofitService();
        imageView = view.findViewById(R.id.detail_image);

        Bundle extras = this.getArguments();

        if (extras != null) {
            book_id = extras.getInt("ebook_id");
        }

        Log.e("detail_book_id", String.valueOf(book_id));


        progressBar=view.findViewById(R.id.progressBar);
        txtbooknameshow = view.findViewById(R.id.txt_book_detail_name);
        txtauthornameshow = view.findViewById(R.id.txt_author_name);
        ratingBar = view.findViewById(R.id.rating_bar_show);
        txtratingbarnumber = view.findViewById(R.id.rating_number);
        txtbookdetail = view.findViewById(R.id.tv_book_detail);
        txtnameinfo = view.findViewById(R.id.txt_name_info);
        txtauthorinfo = view.findViewById(R.id.txt_author_name_info);
        txtpublisherinfo = view.findViewById(R.id.txt_publisher_name_info);
        txttypeinfo = view.findViewById(R.id.tv_book_type_info);
        txteditioninfo = view.findViewById(R.id.tv_book_edition_info);
        txtsizeinfo = view.findViewById(R.id.tv_book_size_info);

        btnseeallcomment = view.findViewById(R.id.btn_see_all_comment);
        recyclercomment = view.findViewById(R.id.recycler_comment);
        adapter = new CommentRecyclerAdapter(getActivity());
        recyclercomment.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclercomment.setAdapter(adapter);

        txtdetail = view.findViewById(R.id.txt_detail);
        txtcontinue = view.findViewById(R.id.txt_continue);

        //binding booktype

        btnseeallcomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getComment();

            }
        });

        recyclercomment.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (currentPage > 0) {
                    getComment();
                }

            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

            }
        });
        getComment();
        bindtoNetwork(view);
        fab_function();
    }

    private void getComment() {
        Log.e("comment_book_id", String.valueOf(book_id));
        if (currentPage > 0) {
            api.getComment(book_id, currentPage).enqueue(new Callback<CommentData>() {
                @Override
                public void onResponse(Call<CommentData> call, Response<CommentData> response) {
                    if (response.isSuccessful()) {
                        Log.e("comment_size", String.valueOf(response.body().data.size()));

                        if (response.body().nextPageUrl == null) {
                            currentPage = -1;
                            Log.e("currentPage", String.valueOf(currentPage));
                        } else {
                            currentPage += 1;
                            Log.e("currentPage:", String.valueOf(currentPage));
                        }


                        adapter.addMoreComments(response.body().data);

                    } else {
                        Log.e("response_fail", response.body().toString());
                    }
                }

                @Override
                public void onFailure(Call<CommentData> call, Throwable t) {

                }
            });
        }
    }

    public void bindtoNetwork(final View view) {

        Api api = retrofitService.getRetrofitService().create(Api.class);
        api.getrecentbookdetail(String.valueOf(book_id)).enqueue(new Callback<RecentBookDetailResponce>() {
            @Override
            public void onResponse(Call<RecentBookDetailResponce> call, Response<RecentBookDetailResponce> response) {

                if (response.isSuccessful()) {
                    Log.e("response","success");

                    if (sharedPreferences.getString(PREFERENCE_KEY,"").equals("z")){

                        txtbooknameshow.setText(Rabbit.uni2zg(response.body().bookDetailBooks.bookTitle));
                        txtnameinfo.setText(Rabbit.uni2zg(response.body().bookDetailBooks.bookTitle));
                        txtauthorinfo.setText(Rabbit.uni2zg(response.body().bookDetailBooks.author));
                        txtpublisherinfo.setText(Rabbit.uni2zg(String.valueOf(response.body().bookDetailBooks.detailBookPublisher.publisherName)));
                        txteditioninfo.setText(Rabbit.uni2zg(String.valueOf(response.body().bookDetailBooks.detailBookEdition.editionName)));
                        txtsizeinfo.setText(Rabbit.uni2zg(String.valueOf(response.body().bookDetailBooks.ebook_size)));
                        txtdetail.setText(Rabbit.uni2zg(String.valueOf(response.body().bookDetailBooks.bookDescription)));
                        txttypeinfo.setText(Rabbit.uni2zg(response.body().bookDetailBooks.category));

                    }else if (sharedPreferences.getString(PREFERENCE_KEY,"").equals("u")){
                        txtbooknameshow.setText(Rabbit.zg2uni(response.body().bookDetailBooks.bookTitle));
                        txtnameinfo.setText(Rabbit.zg2uni(response.body().bookDetailBooks.bookTitle));
                        txtauthorinfo.setText(Rabbit.zg2uni(response.body().bookDetailBooks.author));
                        txtpublisherinfo.setText(Rabbit.zg2uni(String.valueOf(response.body().bookDetailBooks.detailBookPublisher.publisherName)));
                        txteditioninfo.setText(Rabbit.zg2uni(String.valueOf(response.body().bookDetailBooks.detailBookEdition.editionName)));
                        txtsizeinfo.setText(Rabbit.zg2uni(String.valueOf(response.body().bookDetailBooks.ebook_size)));
                        txtdetail.setText(Rabbit.zg2uni(String.valueOf(response.body().bookDetailBooks.bookDescription)));
                        txttypeinfo.setText(Rabbit.zg2uni(response.body().bookDetailBooks.category));

                    }else {

                        txtbooknameshow.setText(response.body().bookDetailBooks.bookTitle);
                        txtnameinfo.setText(response.body().bookDetailBooks.bookTitle);
                        txtauthorinfo.setText(response.body().bookDetailBooks.author);
                        txtpublisherinfo.setText(String.valueOf(response.body().bookDetailBooks.detailBookPublisher.publisherName));
                        txteditioninfo.setText(String.valueOf(response.body().bookDetailBooks.detailBookEdition.editionName));
                        txtsizeinfo.setText(String.valueOf(response.body().bookDetailBooks.ebook_size));
                        txtdetail.setText(String.valueOf(response.body().bookDetailBooks.bookDescription));
                        txttypeinfo.setText(response.body().bookDetailBooks.category);

                    }


                    txtcontinue.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            v.setVisibility(View.GONE);
                        }
                    });

                    Picasso.get().load(BASE_URL + "/api/image/book/" + response.body().bookDetailBooks.bookCoverImgUrl).into(imageView);
                    pdfLink=response.body().bookDetailBooks.e_book_download;
                    pdfName=response.body().bookDetailBooks.bookTitle;

                }
            }

            @Override
            public void onFailure(Call<RecentBookDetailResponce> call, Throwable t) {
                Log.e("ondetailfailure", t.toString());
            }
        });

    }

    void  displayCommentDialog() {

        final Comment bookComment = new Comment();
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.comment_dialog, null);
        dialogBuilder.setView(dialogView);
        final RatingBar ratingBarInput = dialogView.findViewById(R.id.rating_bar_input);
        final EditText etUserName = dialogView.findViewById(R.id.et_name);
        final EditText eTComment = dialogView.findViewById(R.id.et_comment);
        final Button btnCommit = dialogView.findViewById(R.id.btn_commit);
        final double rating = 0.0;


        final AlertDialog b = dialogBuilder.create();
        b.setCanceledOnTouchOutside(true);
        b.show();

        ratingBarInput.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                String s = String.valueOf(v);
                bookComment.setRating(s);
            }
        });


        btnCommit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                b.dismiss();
                if (etUserName.getText().toString().isEmpty()) {
                    Toast.makeText(getContext(), "Username is required!", Toast.LENGTH_LONG);
                    return;
                } else {
                    bookComment.setUsername(etUserName.getText().toString());
                }

                if (eTComment.getText().toString().isEmpty()) {
                    Toast.makeText(getContext(), "Comment is required!", Toast.LENGTH_LONG);
                    return;
                } else {
                    bookComment.setComment(eTComment.getText().toString());
                }


                JSONObject jsonobject = new JSONObject();
                JSONArray jsonArray = new JSONArray();


                try {
                    jsonobject.put("book_id", book_id);
                    jsonobject.put("rating", bookComment.getRating());
                    jsonobject.put("comment", bookComment.getComment());
                    jsonobject.put("username", bookComment.getUsername());
                    jsonobject.put("approve", 1);

                    Log.e("jsonobject", jsonobject.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                jsonArray.put(jsonobject);


                RetrofitService service = new RetrofitService();
                Api api = service.getRetrofitService().create(Api.class);

                api.postComment(jsonobject).enqueue(new Callback<CommentResponse>() {
                    @Override
                    public void onResponse(Call<CommentResponse> call, Response<CommentResponse> response) {
                        if (response.isSuccessful()) {
                            Log.e("response", response.body().success.toString());
                            Toast.makeText(getContext(),"Upload Comment Successfully.",Toast.LENGTH_SHORT).show();


                        } else {
                            Log.e("error", response.toString());
                        }
                    }

                    @Override
                    public void onFailure(Call<CommentResponse> call, Throwable t) {

                        Log.e("onfailure:", t.toString());
                    }
                });

            }


        });
    }



    private boolean writeResponseBodyToDisk(ResponseBody body) {
        try {
            // todo change the file location/name according to your needs
            File futureStudioIconFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),pdfName+".pdf");

            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];

                long fileSize = body.contentLength();
                Log.e("body.contentLength",String.valueOf(fileSize));
                long fileSizeDownloaded = 0;

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(futureStudioIconFile);

                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);

                    fileSizeDownloaded += read;

//                    progressBar.setProgress(read);
                    Log.e("readProgress",String.valueOf(read));

                    Toast.makeText(getContext(), "Downloading...", Toast.LENGTH_LONG).show();
                }

                Toast.makeText(getContext(), "Downloaded", Toast.LENGTH_SHORT).show();

                outputStream.flush();

                return true;
            } catch (IOException e) {
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return false;
        }
    }
}