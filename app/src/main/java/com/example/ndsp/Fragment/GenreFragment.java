package com.example.ndsp.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.ndsp.Adapter.GenreListAdapter;
import com.example.ndsp.ApiInterface.Api;
import com.example.ndsp.Interface.EndlessRecyclerOnScrollListener;
import com.example.ndsp.Interface.OnClickRecyclerItemListener;
import com.example.ndsp.Pojo.GenreListResponse;
import com.example.ndsp.R;
import com.example.ndsp.RetrofitService.RetrofitService;
import com.example.ndsp.model.Genre;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class GenreFragment extends Fragment implements OnClickRecyclerItemListener {

    private RecyclerView recyclerView;
    private RetrofitService service;
    private GenreListAdapter adapter;
    private Api genreListApi;
    private int currentPage=1;


    public GenreFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_genre, container, false);
        initGenreList(view);
        return view;
    }

    private void initGenreList(View view) {

        recyclerView = view.findViewById(R.id.genre_list);
        service = new RetrofitService();
        genreListApi = service.getRetrofitService().create(Api.class);
        adapter = new GenreListAdapter(this);
        LinearLayoutManager layoutManager=new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        Bundle bundle=this.getArguments();
        if(bundle!=null){
            String usrInput=bundle.getString("user_input");
            getSearchItem(usrInput);

        }
        else {

            recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(layoutManager) {
                @Override
                public void onLoadMore(int current_page) {
                    getGenreList(current_page);
                }
            });
            getGenreList(currentPage);
        }

    }

    private void getSearchItem(String usrInput) {

        genreListApi.getSearchGenre(usrInput).enqueue(new Callback<List<Genre>>() {
            @Override
            public void onResponse(Call<List<Genre>> call, Response<List<Genre>> response) {
                if(response.isSuccessful()){
                    adapter.addData(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Genre>> call, Throwable t) {

            }
        });
    }

    private void getGenreList(final int currentPage) {

        genreListApi.getGenreList(currentPage).enqueue(new Callback<GenreListResponse>() {
            @Override
            public void onResponse(Call<GenreListResponse> call, Response<GenreListResponse> response) {

                Log.e("currentPage",String.valueOf(currentPage));

                if (response.isSuccessful()) {
                    adapter.addData(response.body().genres);
                }

            }

            @Override
            public void onFailure(Call<GenreListResponse> call, Throwable t) {

            }
        });


    }


    @Override
    public void onItemClick(int position) {
        GenreListFragment genreListFragment = new GenreListFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("genre_id", position);
        genreListFragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, genreListFragment).commit();
    }
}
