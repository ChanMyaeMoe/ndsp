package com.example.ndsp.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ndsp.Adapter.PublisherAdapter;
import com.example.ndsp.ApiInterface.Api;
import com.example.ndsp.Holder.Publisherholders;
import com.example.ndsp.Interface.EndlessRecyclerOnScrollListener;
import com.example.ndsp.Pojo.PublisherDetailResponse;
import com.example.ndsp.R;
import com.example.ndsp.RetrofitService.RetrofitService;
import com.example.ndsp.model.PublisherDetail;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PublisherListFragment extends Fragment implements Publisherholders.OnPublisherClickListener {

    private RetrofitService retrofitService;
    private RecyclerView recyclerView;
    private PublisherAdapter publisherAdapter;
    private int publisherId;
    private LinearLayoutManager layoutManager;
    private int currentPage=1;
    private  Api publisherDetailApi;

    public PublisherListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_publisher_list, container, false);
        initPublisher(view);
        return view;
    }

    public void initPublisher(View view){

        retrofitService=new RetrofitService();
        publisherDetailApi = retrofitService.getRetrofitService().create(Api.class);

        recyclerView=view.findViewById(R.id.publisher_recycler);
        publisherAdapter=new PublisherAdapter(this,getContext());

        layoutManager=new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(publisherAdapter);

        Bundle bundle=this.getArguments();
        if (bundle !=null){
            publisherId=bundle.getInt("publisher_id");
        }

        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                getPublisherDetail(current_page);
            }
        });

        getPublisherDetail(currentPage);
    }
    private void getPublisherDetail(int currentPage) {

        Log.e("currentPage",String.valueOf(currentPage));
        publisherDetailApi.getPublisherDetail(publisherId,currentPage).enqueue(new Callback<PublisherDetailResponse>() {
            @Override
            public void onResponse(Call<PublisherDetailResponse> call, Response<PublisherDetailResponse> response) {
                if(response.isSuccessful()){

                    publisherAdapter.addItem(response.body().publishers);

                }
            }

            @Override
            public void onFailure(Call<PublisherDetailResponse> call, Throwable t) {

                Log.e("onfailure", t.toString());
            }
        });


    }


    @Override
    public void onPublisherClick(int id) {
        FragmentRecentBookDetail fragmentRecentBookDetail=new FragmentRecentBookDetail();
        Bundle bundle=new Bundle();
        bundle.putInt("book_id",id);
        fragmentRecentBookDetail.setArguments(bundle);
        FragmentTransaction fragmentTransaction=getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_container,fragmentRecentBookDetail).commit();

        Log.e("book_id", String.valueOf(id));
    }
}
