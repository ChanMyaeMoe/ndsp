package com.example.ndsp.Fragment;


import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.ndsp.Adapter.EbookListAdapter;
import com.example.ndsp.ApiInterface.Api;
import com.example.ndsp.Interface.EndlessRecyclerOnScrollListener;
import com.example.ndsp.Interface.OnClickRecyclerItemListener;
import com.example.ndsp.Pojo.BookByCategoryResponse;
import com.example.ndsp.Pojo.CategoryAll;
import com.example.ndsp.Pojo.Category;
import com.example.ndsp.R;
import com.example.ndsp.RetrofitService.RetrofitService;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class EbookListFragment extends Fragment implements OnClickRecyclerItemListener {

    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private RetrofitService service;
    private Api api;
    private EbookListAdapter adapter;
    private int page=1;
    private int categoryListID;
    public EbookListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =inflater.inflate(R.layout.fragment_ebook_list, container, false);
        initFragment(view);

        return view;
    }

    private void initFragment(View view) {
        recyclerView=view.findViewById(R.id.recyclerView);
        layoutManager=new LinearLayoutManager(getContext());
        service=new RetrofitService();
        api=service.getRetrofitService().create(Api.class);
        adapter=new EbookListAdapter(this);

        Bundle bundle=this.getArguments();
        if (bundle!=null){
            categoryListID=bundle.getInt("category_id");
        }

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                getBookList(current_page);
            }
        });

      getBookList(page);
    }

    private void getBookList(int current_page) {
        api.getEbookListByCategory(categoryListID,current_page).enqueue(new Callback<BookByCategoryResponse>() {
            @Override
            public void onResponse(Call<BookByCategoryResponse> call, Response<BookByCategoryResponse> response) {
                if(response.isSuccessful()){
                    adapter.addData(response.body().bookList);
                }
            }

            @Override
            public void onFailure(Call<BookByCategoryResponse> call, Throwable t) {

            }
        });
    }


    @Override
    public void onItemClick(int position) {

            EbookDetailFragment fragment = new EbookDetailFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("ebook_id", position);
            fragment.setArguments(bundle);
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame_container, fragment).commit();
    }
}
