package com.example.ndsp.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ndsp.Adapter.CategoryItemAdapter;
import com.example.ndsp.ApiInterface.Api;
import com.example.ndsp.Holder.CategoryItemHolder;
import com.example.ndsp.Interface.EndlessRecyclerOnScrollListener;
import com.example.ndsp.Pojo.BookByCategoryResponse;
import com.example.ndsp.Pojo.BookListByCategoryDataResponse;
import com.example.ndsp.Pojo.BookListByCategoryResponse;
import com.example.ndsp.R;
import com.example.ndsp.RetrofitService.RetrofitService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryItemFragment extends Fragment implements CategoryItemHolder.OnItemClickListener {
    private RetrofitService retrofitService;
    private RecyclerView recyclerView;
    private CategoryItemAdapter adapter;
    private LinearLayoutManager manager;
    private int categoryListID;
    private int currentPage=1;
    private   Api api;

    public CategoryItemFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_category_item, container, false);
        initResource(view);
        return view;
    }

    public void initResource(View view){
        retrofitService=new RetrofitService();
        api=retrofitService.getRetrofitService().create(Api.class);
        recyclerView=view.findViewById(R.id.recycler_category_list);
        manager=new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);
        adapter=new CategoryItemAdapter( this,getContext());
        recyclerView.setAdapter(adapter);

        Bundle bundle=this.getArguments();
        if (bundle!=null){
            categoryListID=bundle.getInt("category_id");
        }

        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(manager) {
            @Override
            public void onLoadMore(int current_page) {
                getBookList(current_page);
            }
        });
        getBookList(currentPage);

    }

    private void getBookList(int currentPage){

        Log.e("currentPage",String.valueOf(currentPage));
        api.getbooklistcategory(categoryListID,currentPage).enqueue(new Callback<BookByCategoryResponse>() {
            @Override
            public void onResponse(Call<BookByCategoryResponse> call, Response<BookByCategoryResponse> response) {
                if(response.isSuccessful()){
                    adapter.addItem(response.body().bookList);
                }
            }

            @Override
            public void onFailure(Call<BookByCategoryResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void onitemclicklistener(int id) {
        FragmentRecentBookDetail fragmentRecentBookDetail=new FragmentRecentBookDetail();
        Bundle bundle=new Bundle();
        bundle.putInt("book_id",id);
        fragmentRecentBookDetail.setArguments(bundle);
        FragmentTransaction fragmentTransaction=getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_container,fragmentRecentBookDetail).commit();
        Log.e("book_list_id",String.valueOf(id));
    }
}