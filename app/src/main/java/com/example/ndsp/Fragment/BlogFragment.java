package com.example.ndsp.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ndsp.Adapter.BlogAdapter;
import com.example.ndsp.ApiInterface.Api;
import com.example.ndsp.Interface.EndlessRecyclerOnScrollListener;
import com.example.ndsp.Interface.OnClickRecyclerItemListener;
import com.example.ndsp.Pojo.BlogResponse;
import com.example.ndsp.R;
import com.example.ndsp.RetrofitService.RetrofitService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class BlogFragment extends Fragment implements OnClickRecyclerItemListener {

    private RecyclerView recyclerView;
    private BlogAdapter adapter;
    private int page=1;
    private LinearLayoutManager layoutManager;
    private RetrofitService service;
    private Api api;
    public BlogFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view=inflater.inflate(R.layout.fragment_blog, container, false);
        initFragment(view);
        return view;
    }

    private void initFragment(View view) {
        recyclerView=view.findViewById(R.id.recyclerView);
        service=new RetrofitService();
        api=service.getRetrofitService().create(Api.class);
        adapter=new BlogAdapter(this);
        layoutManager=new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        getBlogs(page);

        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                getBlogs(current_page);
            }
        });
    }

    private void getBlogs(int page) {

        api.getArticleLists().enqueue(new Callback<BlogResponse>() {
            @Override
            public void onResponse(Call<BlogResponse> call, Response<BlogResponse> response) {
                if(response.isSuccessful()){

                    adapter.addData(response.body().blogs);
                    Log.e("blogsize",String.valueOf(response.body().blogs.size()));
                }
            }

            @Override
            public void onFailure(Call<BlogResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void onItemClick(int position) {

    }
}
