package com.example.ndsp.Fragment;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ndsp.Adapter.OrderAdapter;
import com.example.ndsp.Adapter.SpinnerAdapter;
import com.example.ndsp.ApiInterface.Api;
import com.example.ndsp.DataBase.OrderItemDatabaseManagement;
import com.example.ndsp.Interface.OnClickRecyclerItemListener;
import com.example.ndsp.Interface.OrderPresenter;
import com.example.ndsp.Pojo.OrderBook;
import com.example.ndsp.Pojo.OrderResponse;
import com.example.ndsp.Pojo.User;
import com.example.ndsp.R;
import com.example.ndsp.RetrofitService.RetrofitService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class FragmentCart extends Fragment implements OnClickRecyclerItemListener,View.OnClickListener {

    private OrderItemDatabaseManagement database;
    private ArrayList<OrderBook> orderBooks;
    private RecyclerView cartRecycler;
    private FrameLayout frameEmpty;
    private LinearLayout frameCart;
    private String email, phoneNumber, city, address;
    private OrderPresenter presenter;
    private TextView qty, charges, amount;
    private Button confirm, order;
    private OrderAdapter adapter;
    private LinearLayoutManager layoutManager;
    private int totalCount, totalPrice;
    private int city_id=-1;
    private User user;
    public FragmentCart() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_fragment_cart, container, false);

        initFragment(view);
        return view;
    }

    private void initFragment(View view) {

        qty = view.findViewById(R.id.footer_qty);
        charges = view.findViewById(R.id.footer_delivery);
        amount = view.findViewById(R.id.footer_amount);
        cartRecycler = view.findViewById(R.id.cart_recycler);
        confirm=view.findViewById(R.id.btn_commit_order);
        order=view.findViewById(R.id.btn_order);
        frameCart=view.findViewById(R.id.frame_cart);
        frameEmpty=view.findViewById(R.id.frame_empty);

        layoutManager = new LinearLayoutManager(getContext());
        orderBooks = new ArrayList<>();


        database = new OrderItemDatabaseManagement(getContext());
        database.dbOpen();
        totalCount = database.getAllItemCount();
        totalPrice = database.getTotalPrice();
        orderBooks.addAll(database.getAllItems());
        database.dbClose();
        adapter = new OrderAdapter(this, getContext(), orderBooks);
        cartRecycler.setLayoutManager(layoutManager);
        cartRecycler.setAdapter(adapter);

        qty.setText(String.valueOf(totalCount));
        amount.setText(String.valueOf(totalPrice));

        confirm.setOnClickListener(this);
        order.setOnClickListener(this);

        if(orderBooks.size()==0){
            frameEmpty.setVisibility(View.VISIBLE);
            frameCart.setVisibility(View.GONE);
        }
        else {
            frameEmpty.setVisibility(View.GONE);
            frameCart.setVisibility(View.VISIBLE);
        }

    }




    @Override
    public void onItemClick(int position) {

        database.dbOpen();
        totalCount = database.getAllItemCount();
        totalPrice = database.getTotalPrice();
        orderBooks=database.getAllItems();
        database.dbClose();
        qty.setText(String.valueOf(totalCount));

        if(city_id<0) {
            amount.setText(String.valueOf(totalPrice));
        }
        else if(city_id==1 || city_id==2){
            charges.setText("1000");
            amount.setText(String.valueOf(totalPrice+1000));
        } else if(city_id==3){
            int charge=1000+(300*(totalCount-1));
            charges.setText(String.valueOf(charge));
            amount.setText(String.valueOf(charge+totalPrice));
        }
        adapter.addData(orderBooks);
        if(orderBooks.size()==0){
            frameEmpty.setVisibility(View.VISIBLE);
            frameCart.setVisibility(View.GONE);
        }
        else {
            frameEmpty.setVisibility(View.GONE);
            frameCart.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onClick(View v) {
        if(v==confirm){

                final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
                LayoutInflater inflater = getActivity().getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.user_dialog, null);
                dialogBuilder.setView(dialogView);

                final EditText etEmail = dialogView.findViewById(R.id.et_user_email);
                final EditText etPhone = dialogView.findViewById(R.id.et_user_phone);
                final EditText etAddress = dialogView.findViewById(R.id.et_user_address);
                final Spinner spinCity = dialogView.findViewById(R.id.spinner);
                final Button btnconfirm = dialogView.findViewById(R.id.btn_order_dialog);

                final List<String> cities = new ArrayList<>();
                cities.add("City");
                cities.add("Yangon");
                cities.add("Mandalay");
                cities.add("Others");

                SpinnerAdapter adapter = new SpinnerAdapter(cities, getActivity().getApplicationContext());
                spinCity.setAdapter(adapter);
                Log.e("city", spinCity.getSelectedItem().toString());

                final AlertDialog b = dialogBuilder.create();
                b.setCanceledOnTouchOutside(true);
                b.show();


                btnconfirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (!(etEmail.getText().toString().isEmpty()) && !(etPhone.getText().toString().isEmpty()) && !(etAddress.getText().toString().isEmpty())) {


                            email = etEmail.getText().toString();
                            phoneNumber = etPhone.getText().toString();
                            address = etAddress.getText().toString();
                            city = spinCity.getSelectedItem().toString();
                            Log.e("city:", spinCity.getSelectedItem().toString());

                            if (city.equals("Mandalay")) {
                                city_id = 1;
                            } else if (city.equals("Yangon")) {
                                city_id = 2;
                            } else if (city.equals("Others")) {
                                city_id = 3;
                            }


                            user = new User();
                            user.setEmail(email);
                            user.setPhone(Long.parseLong(phoneNumber));
                            user.setAddress(address);
                            user.setCity_id(city_id);

                        }
                        if(city_id==1 || city_id==2){
                            charges.setText("1000");
                            amount.setText(String.valueOf(totalPrice+1000));
                        }else if(city_id==3){

                            int charge=1000+(300*(totalCount-1));
                            charges.setText(String.valueOf(charge));
                            amount.setText(String.valueOf(charge+totalPrice));
                        }

                        b.dismiss();

                    }
                });



        }
        else if(v==order){

            if(city_id<0){
                Toast.makeText(getContext(),"Confirm your order",Toast.LENGTH_LONG).show();
            }
            else {
                    makeOrder();
            }
        }
    }


    private void makeOrder() {

        JSONObject object = new JSONObject();
        JSONArray orderedBooksArray = new JSONArray();


        if (user.getAddress() == null || user.getEmail() == null || user.getPhone() == 0 || user.getCity_id() == 0) {
            Toast.makeText(getContext(), "Confirm your order", Toast.LENGTH_LONG).show();
            return;
        } else {
            try {
                object.put("email", user.getEmail());
                object.put("phone", user.getPhone());
                object.put("address", user.getAddress());
                object.put("town_id", user.getCity_id());
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

        for (int i = 0; i < orderBooks.size(); i++) {

            JSONObject bookObj = new JSONObject();
            Map<String, String> bookMap = new HashMap<>();


            try {
                bookObj.put("book_id", orderBooks.get(i).getBook_id());
                bookObj.put("qty", String.valueOf(orderBooks.get(i).getBook_qty()));
            } catch (JSONException e) {
                e.printStackTrace();
            }

//                bookObj = new JSONObject(bookMap);

            orderedBooksArray.put(bookObj);
        }


        try {
            object.put("books", orderedBooksArray);
            Log.i("object:", object.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }


        RetrofitService service = new RetrofitService();
        Api api = service.getRetrofitService().create(Api.class);


        api.postOrder(object).enqueue(new Callback<OrderResponse>() {
            @Override
            public void onResponse(Call<OrderResponse> call, Response<OrderResponse> response) {
                if (response.isSuccessful()) {

                    Log.e("response", response.body().isSuccess);
                    database.dbOpen();
                    database.deleteAllItems();
                    database.dbClose();
                    frameEmpty.setVisibility(View.VISIBLE);
                    frameCart.setVisibility(View.GONE);


                }
            }

            @Override
            public void onFailure(Call<OrderResponse> call, Throwable t) {

                Log.e("onfailure:", t.toString());

            }
        });


    }

}
