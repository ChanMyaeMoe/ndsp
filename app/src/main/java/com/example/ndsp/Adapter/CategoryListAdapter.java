package com.example.ndsp.Adapter;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.ndsp.Holder.CategoryListHolder;
import com.example.ndsp.Interface.OnClickRecyclerItemListener;
import com.example.ndsp.Pojo.Category;
import java.util.ArrayList;
import java.util.List;

public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListHolder> {
    private List<Category> arrayList;
    private OnClickRecyclerItemListener listener;

    public CategoryListAdapter(OnClickRecyclerItemListener listener){
        arrayList=new ArrayList<>();
        this.listener=listener;
    }
    @NonNull
    @Override
    public CategoryListHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater=LayoutInflater.from(viewGroup.getContext());
        return CategoryListHolder.create(inflater,viewGroup,listener);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryListHolder categoryListHolder, int i) {

        categoryListHolder.bindData(arrayList.get(i));
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
    public void addItem(List<Category> categories) {

        if(arrayList.size()==0) {
            this.arrayList = categories;
        }else {
            arrayList.addAll(categories);
        }
        notifyDataSetChanged();

    }
}
