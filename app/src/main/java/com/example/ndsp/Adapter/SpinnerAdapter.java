package com.example.ndsp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.ndsp.R;

import java.util.ArrayList;
import java.util.List;

public class SpinnerAdapter extends BaseAdapter {

    LayoutInflater inflater;
    TextView textView;
    List<String> cities=new ArrayList<>();

    public SpinnerAdapter(List<String> cities, Context applicationContext){
        this.cities=cities;
        inflater = (LayoutInflater.from(applicationContext));
    }
    @Override
    public int getCount() {
        return cities.size();
    }

    @Override
    public Object getItem(int position) {
        return cities.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.support_spinner_dropdown_item, null);
        textView=convertView.findViewById(R.id.txt_spinner);
        textView.setText(cities.get(position));
        return convertView;
    }
}
