package com.example.ndsp.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.ndsp.Holder.BlogHolder;
import com.example.ndsp.Interface.OnClickRecyclerItemListener;
import com.example.ndsp.Pojo.Blog;

import java.util.ArrayList;

public class BlogAdapter extends RecyclerView.Adapter<BlogHolder> {

    private OnClickRecyclerItemListener listener;
    private ArrayList<Blog> blogs;

    public BlogAdapter(OnClickRecyclerItemListener listener){
        this.listener=listener;
        blogs=new ArrayList<>();

    }

    @NonNull
    @Override
    public BlogHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater=LayoutInflater.from(viewGroup.getContext());
        return BlogHolder.create(inflater,viewGroup,listener);
    }

    @Override
    public void onBindViewHolder(@NonNull BlogHolder blogHolder, int i) {

        blogHolder.bindData(blogs.get(i));

    }

    @Override
    public int getItemCount() {
        return blogs.size();
    }

    public void addData(ArrayList<Blog> blogs){

        Log.e("blogId",String.valueOf(blogs.get(1).blogId));
        if(blogs.size()==0){
            this.blogs=blogs;
        }else {
            blogs.addAll(blogs);
        }
    }
}
