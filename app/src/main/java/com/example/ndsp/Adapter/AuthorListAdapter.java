package com.example.ndsp.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.ndsp.Holder.AuthorListHolder;
import com.example.ndsp.Interface.OnClickRecyclerItemListener;
import com.example.ndsp.model.Author;

import java.util.ArrayList;
import java.util.List;

public class AuthorListAdapter extends RecyclerView.Adapter<AuthorListHolder> {

    private List<Author> arraylist=new ArrayList<>();
    private OnClickRecyclerItemListener listener;

    public AuthorListAdapter(OnClickRecyclerItemListener listener){
        this.listener=listener;
    }
    @NonNull
    @Override
    public AuthorListHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        LayoutInflater inflater=LayoutInflater.from(viewGroup.getContext());
        return AuthorListHolder.create(inflater,viewGroup,listener);
    }

    @Override
    public void onBindViewHolder(@NonNull AuthorListHolder authorListHolder, int i) {

        authorListHolder.bindData(arraylist.get(i));
    }

    @Override
    public int getItemCount() {
        return arraylist.size();
    }

    public void addData(List<Author> arraylist){

        if(this.arraylist.size()==0) {
            this.arraylist = arraylist;
        }else {
            this.arraylist.addAll(arraylist);
        }
        notifyDataSetChanged();
    }
}
