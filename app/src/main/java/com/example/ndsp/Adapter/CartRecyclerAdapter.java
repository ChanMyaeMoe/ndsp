package com.example.ndsp.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ndsp.ApiInterface.Api;
import com.example.ndsp.DataBase.OrderItemDatabaseManagement;
import com.example.ndsp.Interface.CardDatabaseInterface;
import com.example.ndsp.Interface.OrderNofity;
import com.example.ndsp.Pojo.Order;
import com.example.ndsp.Pojo.OrderBook;
import com.example.ndsp.Pojo.OrderResponse;
import com.example.ndsp.Pojo.Order_Books;
import com.example.ndsp.Pojo.Rabbit;
import com.example.ndsp.Pojo.TopTenResponse;
import com.example.ndsp.Pojo.User;
import com.example.ndsp.R;
import com.example.ndsp.RetrofitService.RetrofitService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_FOOTER = 0;
    private static final int TYPE_ITEM = 1;
    Context context;
    private int qty, pri;
    private ArrayList<OrderBook> orderBooks = new ArrayList<>();
    private OrderItemDatabaseManagement database;
    private CardDatabaseInterface.CartDataBaseInterface cartDataBaseInterface;
    private OrderNofity.OrderNotify orderNotify;
    private Typeface roboto_thin, roboto_light, roboto_regular;
    private String city = "city";
    private OrderItemDatabaseManagement databaseManagement;
    private User user = new User();
    public static final String LANGUAGE_PREFERENCE = "lan_pref", PREFERENCE_KEY = "lan_key";
    private SharedPreferences sharedPreferences;


    public CartRecyclerAdapter(Context context, ArrayList<OrderBook> orderBooks, CardDatabaseInterface.CartDataBaseInterface cartDataBaseInterface, OrderNofity.OrderNotify orderNotify) {

        this.context = context;
        this.orderBooks = orderBooks;
        database = new OrderItemDatabaseManagement(context);
        this.cartDataBaseInterface = cartDataBaseInterface;
        this.orderNotify = orderNotify;
        database = new OrderItemDatabaseManagement(context);

        this.context = context;
        sharedPreferences = context.getSharedPreferences(LANGUAGE_PREFERENCE, Context.MODE_PRIVATE);
        qtyOrderBook();
    }

    private int qtyOrderBook() {
        int q = 0;
        for (int i = 0; i < orderBooks.size(); i++) {
            q += orderBooks.get(i).getBook_qty();
        }
        return q;
    }

    private int priceOrderBook() {
        int amount = 0;
        for (int i = 0; i < orderBooks.size(); i++) {

            amount += orderBooks.get(i).getBook_qty() * orderBooks.get(i).getBook_price();
        }
        return amount;
    }

    public void setDeliveriedCity(String city) {
        this.city = city;
        notifyDataSetChanged();
    }

    public void addUserInfo(User user) {

        this.user = user;
        notifyDataSetChanged();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        if (viewType == TYPE_FOOTER) {
            View itemView = LayoutInflater.from(context).inflate(R.layout.footer, parent, false);
            return new CartRecyclerAdapter.FooterViewHolder(itemView);
        } else if (viewType == TYPE_ITEM) {
            View item = LayoutInflater.from(context).inflate(R.layout.order_list_item, parent, false);
            return new CartRecyclerAdapter.ItemViewHolder(item);
        } else
            return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        Log.i("onBind", "reachTal");

        final int[] q = {1};

        if (holder instanceof ItemViewHolder) {

            q[0] = orderBooks.get(holder.getAdapterPosition()).getBook_qty();

            ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
            setCustomFontsForHeaderTxtView(itemViewHolder);
            Random rand = new Random();

            itemViewHolder.color.setBackgroundColor(Color.rgb(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255)));
            itemViewHolder.color.setBackgroundColor(Color.rgb(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255)));


            if (sharedPreferences.getString(PREFERENCE_KEY, "").equals("z")) {

                itemViewHolder.title.setText(Rabbit.uni2zg(orderBooks.get(holder.getAdapterPosition()).getBook_name()));

            } else if (sharedPreferences.getString(PREFERENCE_KEY, "").equals("u")) {

                itemViewHolder.title.setText(Rabbit.zg2uni(orderBooks.get(holder.getAdapterPosition()).getBook_name()));
            } else {
                itemViewHolder.title.setText(orderBooks.get(holder.getAdapterPosition()).getBook_name());
            }


//            itemViewHolder.title.setTextColor(categoryColors[n]);
            itemViewHolder.price.setText(String.valueOf(orderBooks.get(holder.getAdapterPosition()).getBook_price()));
            itemViewHolder.qty.setText(String.valueOf(orderBooks.get(holder.getAdapterPosition()).getBook_qty()));
            itemViewHolder.editIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    orderNotify.deleteOrderItem(orderBooks.get(holder.getAdapterPosition()).getBook_id(), orderBooks.get(holder.getAdapterPosition()).getBook_name());
                    notifyItemRemoved(holder.getAdapterPosition());
//                    orderBooks.remove(holder.getAdapterPosition());
                    notifyItemChanged(orderBooks.size());
                }
            });
            itemViewHolder.add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    q[0]++;
                    qty++;
                    pri = orderBooks.get(holder.getAdapterPosition()).getBook_price() * qty;
                    ((ItemViewHolder) holder).qty.setText(String.valueOf(q[0]));

                    database.dbOpen();
                    database.updateQuantity(String.valueOf(q[0]), orderBooks.get(holder.getAdapterPosition()).getBook_id());
                    database.dbClose();
                    notifyItemChanged(orderBooks.size());
                }

            });
            itemViewHolder.sub.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if ((q[0] > 1)) {
                        q[0]--;
                        qty--;
                        pri = orderBooks.get(holder.getAdapterPosition()).getBook_price() * qty;
                        ((ItemViewHolder) holder).qty.setText(String.valueOf(q[0]));
                        database.dbOpen();
                        database.updateQuantity(String.valueOf(q[0]), orderBooks.get(holder.getAdapterPosition()).getBook_id());
                        database.dbClose();

                    } else if (q[0] == 1) {
                        q[0] = 1;
                        database.dbOpen();
                        database.updateQuantity("1", orderBooks.get(holder.getAdapterPosition()).getBook_id());
                        database.dbClose();
                        ((ItemViewHolder) holder).qty.setText(String.valueOf(q[0]));

                    }

                    notifyItemChanged(orderBooks.size());
                }
            });
        } else if (holder instanceof FooterViewHolder) {

            Log.i("footer", "reachTal");

            final FooterViewHolder footerHolder = (FooterViewHolder) holder;
            setCustomFontsForFooterTxtView(footerHolder);

            int qq, pp;
            qq = qtyOrderBook();
            pp = priceOrderBook();

            footerHolder.footerQty.setText(String.valueOf(qty + qq));
            footerHolder.footerAmount.setText(String.valueOf(pri + pp));//total amount


            if (city.equals("Others")) {
                int price = 1000 + ((Integer.parseInt(footerHolder.footerQty.getText().toString()) - 1) * 300);
                footerHolder.delivery_charges.setText(String.valueOf(price));
                footerHolder.footerAmount.setText(String.valueOf(Integer.parseInt(footerHolder.delivery_charges.getText().toString()) + pri + pp));
            } else if (city.equals("Yangon") || city.equals("Mandalay")) {
                footerHolder.delivery_charges.setText("1000");
                footerHolder.footerAmount.setText(String.valueOf(1000 + pri + pp));
            }

            footerHolder.btnConfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cartDataBaseInterface.confirmClick();

                }
            });

        }
    }

    private void setCustomFontsForFooterTxtView(FooterViewHolder footerHolder) {
        footerHolder.footerAmount.setTypeface(roboto_light);
        footerHolder.footerQty.setTypeface(roboto_light);
        footerHolder.footer_totalNumber.setTypeface(roboto_regular);
        footerHolder.footer_totalPrice.setTypeface(roboto_regular);
        footerHolder.btnConfirm.setTypeface(roboto_regular);
        footerHolder.footer_kyatNumber.setTypeface(roboto_thin);
        footerHolder.footer_kyatPrice.setTypeface(roboto_thin);
    }

    private void setCustomFontsForHeaderTxtView(ItemViewHolder itemViewHolder) {
        itemViewHolder.title.setTypeface(roboto_light);
        itemViewHolder.price.setTypeface(roboto_regular);
        itemViewHolder.qty.setTypeface(roboto_regular);
        itemViewHolder.kyat.setTypeface(roboto_thin);
    }

    @Override
    public int getItemViewType(int position) {

        if (position != orderBooks.size()) {

            return TYPE_ITEM;
        } else return TYPE_FOOTER;
    }

    @Override
    public int getItemCount() {
        return orderBooks.size() + 1;
    }


    private class ItemViewHolder extends RecyclerView.ViewHolder {

        View color;
        ImageView editIcon;
        TextView title, price, qty, kyat;
        ImageButton add, sub;

        ItemViewHolder(View itemView) {
            super(itemView);
            kyat = itemView.findViewById(R.id.txtView_header_kyats);

            add = itemView.findViewById(R.id.imgBtn_add);
            sub = itemView.findViewById(R.id.imgBtn_substract);
            color = itemView.findViewById(R.id.view_color);
            editIcon = itemView.findViewById(R.id.edit_icon);
            title = itemView.findViewById(R.id.order_book_name);
            price = itemView.findViewById(R.id.order_book_price);
            qty = itemView.findViewById(R.id.order_book_qty);
        }
    }

    private class FooterViewHolder extends RecyclerView.ViewHolder {
        Button btnConfirm, btnOrder;
        TextView footerQty, footerAmount,
                footer_totalNumber, footer_totalPrice,
                footer_kyatNumber, footer_kyatPrice, delivery_charges;

        FooterViewHolder(View view) {
            super(view);
            footer_totalNumber = view.findViewById(R.id.txtView_footer_totalNumber);
            footer_totalPrice = view.findViewById(R.id.txtView_footer_totalPrice);
            btnConfirm = view.findViewById(R.id.btn_commit_order);
            footerAmount = view.findViewById(R.id.footer_amount);
            footerQty = view.findViewById(R.id.footer_qty);
            footer_kyatNumber = view.findViewById(R.id.txtView_footer_kyatNumber);
            footer_kyatPrice = view.findViewById(R.id.txtView_footer_kyatPrice);
            delivery_charges = view.findViewById(R.id.footer_delivery);
            btnOrder = view.findViewById(R.id.btn_order);

            btnOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    makeOrder();
                }
            });
        }

        private void makeOrder() {

            JSONObject object = new JSONObject();
            JSONArray orderedBooksArray = new JSONArray();

            ArrayList<OrderBook> bookList = new ArrayList<>();
            database.dbOpen();
            bookList.addAll(database.getAllItems());
            database.dbClose();


//                    Map wholeMap = new HashMap();
//
//                    wholeMap.put("email",user.getEmail());
//                    wholeMap.put("phone",user.getPhone());
//                    wholeMap.put("address",user.getAddress());
//                    wholeMap.put("town_id",1);
//
//                    object=new JSONObject(wholeMap);


            if (user.getAddress() == null || user.getEmail() == null || user.getPhone() == 0 || user.getCity_id() == 0) {
                Toast.makeText(context, "Confirm your order", Toast.LENGTH_LONG).show();
                return;
            } else {
                try {
                    object.put("email", user.getEmail());
                    object.put("phone", user.getPhone());
                    object.put("address", user.getAddress());
                    object.put("town_id", user.getCity_id());
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            for (int i = 0; i < bookList.size(); i++) {

                JSONObject bookObj = new JSONObject();
                Map<String, String> bookMap = new HashMap<>();


                try {
                    bookObj.put("book_id", bookList.get(i).getBook_id());
                    bookObj.put("qty", String.valueOf(bookList.get(i).getBook_qty()));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

//                bookObj = new JSONObject(bookMap);

                orderedBooksArray.put(bookObj);
            }


            try {
                object.put("books", orderedBooksArray);
                Log.i("object:", object.toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }


            RetrofitService service = new RetrofitService();
            Api api = service.getRetrofitService().create(Api.class);


            api.postOrder(object).enqueue(new Callback<OrderResponse>() {
                @Override
                public void onResponse(Call<OrderResponse> call, Response<OrderResponse> response) {
                    if (response.isSuccessful()) {

                        Log.e("response", response.body().isSuccess);
                        database.dbOpen();
                        database.deleteAllItems();
                        database.dbClose();


                    }
                }

                @Override
                public void onFailure(Call<OrderResponse> call, Throwable t) {

                    Log.e("onfailure:", t.toString());

                }
            });


        }


    }

}
