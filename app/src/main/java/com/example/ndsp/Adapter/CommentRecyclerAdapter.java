package com.example.ndsp.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import com.example.ndsp.Pojo.DataComment;
import com.example.ndsp.Pojo.Rabbit;
import com.example.ndsp.R;

import java.util.ArrayList;
import java.util.List;

public class CommentRecyclerAdapter extends RecyclerView.Adapter<CommentRecyclerAdapter.MyViewHolder> {

    Context context;
    private List<DataComment> comments = new ArrayList<>();
    public static final String LANGUAGE_PREFERENCE = "lan_pref", PREFERENCE_KEY = "lan_key";
    private SharedPreferences sharedPreferences;

    //constructor
    public CommentRecyclerAdapter(Context context) {
        this.context = context;
        this.context=context;
        sharedPreferences =context.getSharedPreferences(LANGUAGE_PREFERENCE, Context.MODE_PRIVATE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView =  LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_item,parent,false);
        return  new CommentRecyclerAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        if (sharedPreferences.getString(PREFERENCE_KEY,"").equals("z")){

            holder.tvName.setText(Rabbit.uni2zg(comments.get(position).username));
            holder.tvComment.setText(Rabbit.uni2zg(comments.get(position).comment));
        }else if (sharedPreferences.getString(PREFERENCE_KEY,"").equals("u")){

            holder.tvName.setText(Rabbit.zg2uni(comments.get(position).username));
            holder.tvComment.setText(Rabbit.zg2uni(comments.get(position).comment));
        }else {
            holder.tvName.setText(comments.get(position).username);
            holder.tvComment.setText(comments.get(position).comment);
        }


        holder.tvDateComment.setText(comments.get(position).created_at);
        holder.tvRaingComment.setRating(Float.parseFloat(comments.get(position).rating));
    }

    @Override
    public int getItemCount() {
        return comments.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvName,tvComment,tvDateComment;
        RatingBar tvRaingComment;
        MyViewHolder(View itemView) {
            super(itemView);

            tvComment = itemView.findViewById(R.id.comment_user_comment);
            tvName = itemView.findViewById(R.id.comment_user_name);
            tvRaingComment = itemView.findViewById(R.id.rating_comment);
            tvDateComment = itemView.findViewById(R.id.date_comment);
        }
    }

    public void addMoreComments(List<DataComment>comments){
        this.comments.addAll(comments);
        notifyDataSetChanged();
    }
}
