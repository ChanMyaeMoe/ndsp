package com.example.ndsp.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.ndsp.Holder.GenreHolder;
import com.example.ndsp.model.Genre;
import com.example.ndsp.model.GenreDetail;

import java.util.ArrayList;
import java.util.List;

public class GenreAdapter extends RecyclerView.Adapter<GenreHolder> {
    private List<GenreDetail> arrayList;
    private GenreHolder.OnGenreClickListener onGenreClickListener;
    private Context context;

    public GenreAdapter(GenreHolder.OnGenreClickListener listener,Context context) {

        arrayList = new ArrayList<>();
        this.onGenreClickListener = listener;
        this.context=context;

    }


    @NonNull
    @Override
    public GenreHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        return GenreHolder.create(layoutInflater, viewGroup, onGenreClickListener,context);
    }

    @Override
    public void onBindViewHolder(@NonNull GenreHolder genreHolder, int i) {
        genreHolder.bindData(arrayList.get(i));

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public void addItem(List<GenreDetail> genreDetails) {

        if (arrayList.size() == 0) {
            this.arrayList = genreDetails;
        } else {
            arrayList.addAll(genreDetails);
        }
        notifyDataSetChanged();

    }

}
