package com.example.ndsp.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.ndsp.Holder.Publisherholders;
import com.example.ndsp.model.PublisherDetail;

import java.util.ArrayList;
import java.util.List;

public class PublisherAdapter extends RecyclerView.Adapter<Publisherholders> {

    private List<PublisherDetail> publisherLists ;
    private Publisherholders.OnPublisherClickListener listener;
    private Context context;

    public PublisherAdapter(Publisherholders.OnPublisherClickListener listener,Context context){

        publisherLists =new ArrayList<>();
        this.listener=listener;
        this.context=context;
    }

    @NonNull
    @Override
    public Publisherholders onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        LayoutInflater inflater=LayoutInflater.from(viewGroup.getContext());
        return Publisherholders.create(inflater,viewGroup,listener,context);
    }

    @Override
    public void onBindViewHolder(@NonNull Publisherholders publisherholder, int i) {

        publisherholder.bindData(publisherLists.get(i));
    }


    @Override
    public int getItemCount() {
        return publisherLists.size();
    }


    public void addItem(List<PublisherDetail> publisher){

        if(publisherLists.size()==0) {
            this.publisherLists = publisher;
        }
        else {
            publisherLists.addAll(publisher);
        }
        notifyDataSetChanged();

    }

}


