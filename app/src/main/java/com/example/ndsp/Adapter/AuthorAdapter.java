package com.example.ndsp.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ndsp.Fragment.AuthorListFragment;
import com.example.ndsp.Holder.AuthorHolder;

import com.example.ndsp.R;
import com.example.ndsp.model.AuthorDetail;

import java.util.ArrayList;
import java.util.List;

public class AuthorAdapter extends RecyclerView.Adapter<AuthorHolder>{

    private List<AuthorDetail> authorLists;
    private AuthorHolder.OnAuthorClickListener listener;
    private Context context;

    public AuthorAdapter(AuthorHolder.OnAuthorClickListener listener,Context context) {

        authorLists = new ArrayList<>();
        this.listener = listener;
        this.context=context;
    }

    @NonNull
    @Override
    public AuthorHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        LayoutInflater inflater=LayoutInflater.from(viewGroup.getContext());
        return AuthorHolder.create(inflater, viewGroup, listener,context);
    }


    @Override
    public void onBindViewHolder(@NonNull AuthorHolder authorHolder, int i) {

        authorHolder.bindData(authorLists.get(i));

    }

    @Override
    public int getItemCount() {
        return authorLists.size();
    }

    public void addItem(List<AuthorDetail> author){

        if(authorLists.size()==0) {
            this.authorLists = author;
        }
        else {
            authorLists.addAll(author);
        }
        notifyDataSetChanged();
    }
}
