package com.example.ndsp.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.ndsp.Holder.CategoryItemHolder;
import com.example.ndsp.Pojo.BookByCategory;
import com.example.ndsp.Pojo.BookListByCategoryDataResponse;

import java.util.ArrayList;
import java.util.List;

public class CategoryItemAdapter extends RecyclerView.Adapter<CategoryItemHolder> {
    List<BookByCategory> bookList=new ArrayList<>();
    CategoryItemHolder.OnItemClickListener listener;
    Context context;

    public CategoryItemAdapter(CategoryItemHolder.OnItemClickListener listener, Context context) {
        this.listener=listener;
        this.context=context;
    }

    @NonNull
    @Override
    public CategoryItemHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater=LayoutInflater.from(viewGroup.getContext());
        return CategoryItemHolder.create(inflater,viewGroup,listener,context);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryItemHolder categoryItemHolder, int i) {
        categoryItemHolder.bindData(bookList.get(i));

    }

    @Override
    public int getItemCount() {
        return bookList.size();
    }

    public void addItem(List<BookByCategory> bookList){
        if(this.bookList.size()==0) {
            this.bookList = bookList;
        }else {
            this.bookList.addAll(bookList);
        }
        notifyDataSetChanged();
    }

}