package com.example.ndsp.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.ndsp.Holder.PubisherListHolder;
import com.example.ndsp.Interface.OnClickRecyclerItemListener;
import com.example.ndsp.model.Publisher;

import java.util.ArrayList;
import java.util.List;

public class PublisherListAdapter extends RecyclerView.Adapter<PubisherListHolder> {

    private List<Publisher> arrayList;
    private OnClickRecyclerItemListener listener;

    public PublisherListAdapter(OnClickRecyclerItemListener listener) {
        arrayList = new ArrayList<>();
        this.listener = listener;
    }

    @NonNull
    @Override
    public PubisherListHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        return PubisherListHolder.create(inflater, viewGroup, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull PubisherListHolder pubisherListHolder, int i) {
        pubisherListHolder.bindData(arrayList.get(i));
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public void addData(List<Publisher> arrayList){
        if(this.arrayList.size()==0) {
            this.arrayList = arrayList;
        }
        else {
           this.arrayList.addAll(arrayList);
        }
        notifyDataSetChanged();
    }
}
