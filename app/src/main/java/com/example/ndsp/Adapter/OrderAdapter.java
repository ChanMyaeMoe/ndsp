package com.example.ndsp.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.ndsp.Holder.OrderHolder;
import com.example.ndsp.Interface.OnClickRecyclerItemListener;
import com.example.ndsp.Pojo.OrderBook;

import java.util.ArrayList;
import java.util.List;

public class OrderAdapter extends RecyclerView.Adapter<OrderHolder> {

    private List<OrderBook> orderBooks=new ArrayList<>();
    private OnClickRecyclerItemListener listener;
    private Context context;

    public OrderAdapter(OnClickRecyclerItemListener listener, Context context,List<OrderBook> orderBooks){
        this.listener=listener;
        this.context=context;
        this.orderBooks=orderBooks;
    }

    @NonNull
    @Override
    public OrderHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        LayoutInflater inflater=LayoutInflater.from(context);
        return OrderHolder.create(inflater,viewGroup,listener,context);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderHolder orderHolder, int i) {
        orderHolder.bindData(orderBooks.get(i));
    }

    @Override
    public int getItemCount() {
        return orderBooks.size();
    }

    public void addData(List<OrderBook> orderBooks){
        this.orderBooks=orderBooks;
        notifyDataSetChanged();
    }
}
