package com.example.ndsp.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.ndsp.Holder.GenreListHolder;
import com.example.ndsp.Interface.OnClickRecyclerItemListener;
import com.example.ndsp.model.Genre;

import java.util.ArrayList;
import java.util.List;

public class GenreListAdapter extends RecyclerView.Adapter<GenreListHolder> {

    private List<Genre> arrayList;
    private OnClickRecyclerItemListener listener;

    public GenreListAdapter(OnClickRecyclerItemListener listener){
        arrayList=new ArrayList<>();
        this.listener=listener;
    }
    @NonNull
    @Override
    public GenreListHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater=LayoutInflater.from(viewGroup.getContext());
        return GenreListHolder.create(inflater,viewGroup,listener);
    }

    @Override
    public void onBindViewHolder(@NonNull GenreListHolder genreListHolder, int i) {
            genreListHolder.bindData(arrayList.get(i));
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public void addData(List<Genre> arrayList){
        if(this.arrayList.size()==0){
            this.arrayList=arrayList;
        }else {
            this.arrayList.addAll(arrayList);
        }

        notifyDataSetChanged();
    }
}
