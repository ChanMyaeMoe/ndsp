package com.example.ndsp.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.example.ndsp.Holder.SearchHolder;
import com.example.ndsp.Interface.OnClickRecyclerItemListener;
import com.example.ndsp.Pojo.Book;

import java.util.ArrayList;
import java.util.List;

public class SearchAdapter extends RecyclerView.Adapter<SearchHolder> {

    private OnClickRecyclerItemListener listener;
    private List<Book> searchList;

    public SearchAdapter(OnClickRecyclerItemListener listener){
        searchList=new ArrayList<>();
        this.listener=listener;
    }
    @NonNull
    @Override
    public SearchHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        LayoutInflater inflater=LayoutInflater.from(viewGroup.getContext());

        return SearchHolder.create(inflater,viewGroup,listener);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchHolder searchHolder, int i) {
            searchHolder.bindData(searchList.get(i));
    }

    @Override
    public int getItemCount() {
        return searchList.size();
    }

    public void addData(List<Book> bookList){
        this.searchList=bookList;
        notifyDataSetChanged();
    }
}
