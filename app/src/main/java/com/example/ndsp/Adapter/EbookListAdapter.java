package com.example.ndsp.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.ndsp.Holder.EbookListHolder;
import com.example.ndsp.Interface.OnClickRecyclerItemListener;
import com.example.ndsp.Pojo.BookByCategory;

import java.util.ArrayList;
import java.util.List;

public class EbookListAdapter extends RecyclerView.Adapter<EbookListHolder> {
    private List<BookByCategory> bookList;
    private OnClickRecyclerItemListener listener;

    public EbookListAdapter(OnClickRecyclerItemListener listener){

        bookList=new ArrayList<>();
        this.listener=listener;
    }
    @NonNull
    @Override
    public EbookListHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater=LayoutInflater.from(viewGroup.getContext());
        return EbookListHolder.create(inflater,viewGroup,listener,viewGroup.getContext());

    }

    @Override
    public void onBindViewHolder(@NonNull EbookListHolder ebookListHolder, int i) {
        ebookListHolder.bindData(bookList.get(i));

    }

    @Override
    public int getItemCount() {
        return bookList.size();
    }

    public void addData(List<BookByCategory> bookList){

        if(this.bookList.size()==0) {
            this.bookList = bookList;
        }
        else {
            this.bookList.addAll(bookList);
        }
        notifyDataSetChanged();
    }
}
