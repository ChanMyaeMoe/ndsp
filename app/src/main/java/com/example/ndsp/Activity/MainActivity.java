package com.example.ndsp.Activity;

import android.animation.ValueAnimator;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.graphics.drawable.DrawerArrowDrawable;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.example.ndsp.DataBase.OrderItemDatabaseManagement;
import com.example.ndsp.Fragment.AuthorFragment;
import com.example.ndsp.Fragment.BlogDetailDFragment;
import com.example.ndsp.Fragment.BlogFragment;
import com.example.ndsp.Fragment.CategoryItemFragment;
import com.example.ndsp.Fragment.EbookListFragment;
import com.example.ndsp.Fragment.EbookSearchFragment;
import com.example.ndsp.Fragment.EbookSeeAllFragment;
import com.example.ndsp.Fragment.FragmentBlog;
import com.example.ndsp.Fragment.FragmentCart;
import com.example.ndsp.Fragment.FragmentCategory;
import com.example.ndsp.Fragment.FragmentExlplore;
import com.example.ndsp.Fragment.GenreFragment;
import com.example.ndsp.Fragment.PublisherFragment;
import com.example.ndsp.Fragment.BookSearchFragment;
import com.example.ndsp.Fragment.SettingFragment;
import com.example.ndsp.R;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import static com.example.ndsp.Pojo.FragmentInfo.clickInfo;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private ValueAnimator animator;
    private DrawerArrowDrawable drawerArrowDrawable;
    private ImageView plusIcon;
    private Toolbar toolbar;
    private TextView toolbarTitle;
    private LinearLayout layoutData;
    private DrawerLayout drawer;
    private MaterialSearchView searchView;
    private OrderItemDatabaseManagement database;
    private BottomNavigationView bottomNavigationView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        plusIcon = findViewById(R.id.search_icon_plus);

        drawerArrowDrawable = new DrawerArrowDrawable(this);

        loadFragment(new FragmentExlplore());
        initResource();



        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        database = new OrderItemDatabaseManagement(getApplicationContext());

//NavigationDrawer
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //bottomnavigation
        bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener);
//        BottomNavigationViewHelper helper = new BottomNavigationViewHelper();
//        helper.removeShiftMode(bottomNavigationView);

//        drawerArrorFunction();

//        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
//            @Override
//            public void onSearchViewShown() {
//                layoutData.setVisibility(View.GONE);
//                searchConditions();
////            }
//
//            @Override
//            public void onSearchViewClosed() {
//                layoutData.setVisibility(View.VISIBLE);
//
//            }
//        });
    }

    private BottomNavigationView.OnNavigationItemSelectedListener onNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            Fragment fragment;
            switch (menuItem.getItemId()) {

                case R.id.btnnav_explore:
                    fragment = new FragmentExlplore();
                    loadFragment(fragment);
                    return true;
                case R.id.btnnav_category:
                    fragment = new FragmentCategory();
                    loadFragment(fragment);
                    return true;
                case R.id.btnnav_blog:
                    fragment = new BlogDetailDFragment();
                    loadFragment(fragment);
                    return true;
                case R.id.btnnav_cart:
//                    toolbar.setTitle("Profile");
                    fragment = new FragmentCart();
                    loadFragment(fragment);
                    return true;
            }
            return false;
        }
    };
    private Boolean exit = false;

    @Override
    public void onBackPressed() {
        if (exit) {
            super.onBackPressed();
            return;
        }

        try {
            FragmentManager fragmentManager = getSupportFragmentManager();
            Fragment fragment = fragmentManager.findFragmentByTag("HOME");
            if (fragment != null) {
                if (fragment.isVisible()) {
                    this.exit = true;
                    Toast.makeText(this, "Press Back again to Exit", Toast.LENGTH_SHORT).show();
                }
            } else {
                fragment = FragmentExlplore.class.newInstance();
                getFragmentManager().popBackStack();
                fragmentManager.beginTransaction().replace(R.id.frame_container, fragment, "HOME").commit();
            }
        } catch (Exception e) {

        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                exit = false;
            }
        }, 2000);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_view_menu_item, menu);
        MenuItem searchViewItem = menu.findItem(R.id.action_search);
        final SearchView searchViewAndroidActionBar = (SearchView) MenuItemCompat.getActionView(searchViewItem);
        searchViewAndroidActionBar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {


                Log.e("TextSubmit", query);
                searchViewAndroidActionBar.clearFocus();
                getMenuInflater().inflate(R.menu.main, menu);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.e("clickInfo",clickInfo);

                if(clickInfo.equals("category")){
                    FragmentCategory fragment=new FragmentCategory();
                    Bundle bundle=new Bundle();
                    bundle.putString("user_input",newText);
                    fragment.setArguments(bundle);
                    getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, fragment).commit();

                }else if(clickInfo.equals("publisher")){
                    PublisherFragment fragment=new PublisherFragment();
                    Bundle bundle=new Bundle();
                    bundle.putString("user_input",newText);
                    fragment.setArguments(bundle);
                    getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, fragment).commit();

                }else if(clickInfo.equals("author")){

                    AuthorFragment fragment=new AuthorFragment();
                    Bundle bundle=new Bundle();
                    bundle.putString("user_input",newText);
                    fragment.setArguments(bundle);
                    getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, fragment).commit();


                }else if(clickInfo.equals("genre")){

                    GenreFragment fragment=new GenreFragment();
                    Bundle bundle=new Bundle();
                    bundle.putString("user_input",newText);
                    fragment.setArguments(bundle);
                    getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, fragment).commit();


                }else if(clickInfo.equals("book")){

                    BookSearchFragment fragment=new BookSearchFragment();
                    Bundle bundle=new Bundle();
                    bundle.putString("user_input",newText);
                    fragment.setArguments(bundle);
                    getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, fragment).commit();

                }else if(clickInfo.equals("ebook_category")){

                    EbookSeeAllFragment fragment=new EbookSeeAllFragment();
                    Bundle bundle=new Bundle();
                    bundle.putString("user_input",newText);
                    fragment.setArguments(bundle);
                    getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, fragment).commit();

                }else if(clickInfo.equals("ebook")){
                    EbookSearchFragment fragment=new EbookSearchFragment();
                    Bundle bundle=new Bundle();
                    bundle.putString("user_input",newText);
                    fragment.setArguments(bundle);
                    getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, fragment).commit();
                }

                Log.e("TextChange", newText);


                searching(newText);

                return true;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    private void searching(String newText) {

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_category) {

            clickInfo="category";
            FragmentCategory fragmentCategory = new FragmentCategory();
            getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, fragmentCategory).commit();

        } else if (id == R.id.nav_author) {

            clickInfo="author";
            AuthorFragment authorFragment = new AuthorFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, authorFragment).commit();

        } else if (id == R.id.nav_publisher) {

            clickInfo="publisher";
            PublisherFragment publisherFragment = new PublisherFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, publisherFragment).commit();

        } else if (id == R.id.nav_genre) {

            clickInfo="genre";
            GenreFragment genreFragment = new GenreFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, genreFragment).commit();

        } else if (id == R.id.nav_ebook) {

            clickInfo="ebook_category";
            EbookSeeAllFragment fragmentCategory = new EbookSeeAllFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, fragmentCategory).commit();

        } else if (id == R.id.nav_language) {
            SettingFragment settingFragment = new SettingFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, settingFragment).commit();

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.commit();
    }

    private void drawerArrorFunction() {


        drawerArrowDrawable.setSpinEnabled(false);
        drawerArrowDrawable.setColor(getResources().getColor(R.color.burger_white));
//        plusIcon.setImageDrawable(drawerArrowDrawable);
//        plusIcon.setVisibility(View.GONE);
        animator = ValueAnimator.ofFloat(0f, 1f);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.frame_container);
                if ((currentFragment instanceof FragmentExlplore)) {
                    drawerArrowDrawable.setColor(getResources().getColor(R.color.burger_white));

                } else {
                    drawerArrowDrawable.setColor(getResources().getColor(R.color.burger_white));
                }
                drawerArrowDrawable.setProgress((Float) animation.getAnimatedValue());
            }
        });
    }

    public void initResource() {

        clickInfo= clickInfo;

        toolbar = findViewById(R.id.toolbar);

        toolbarTitle = findViewById(R.id.tvToolbarTitle);

        layoutData = findViewById(R.id.frame_for_data);

        searchView = findViewById(R.id.search_view);
//        searchView.setCursorDrawable(R.drawable.custom_cursor);


    }

    //confirm for app existing
    private void showAlertDialog() {
        Button btn_no, btn_yes;
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.exit_dialog, null);
        dialogBuilder.setView(dialogView);


        final AlertDialog b = dialogBuilder.create();
        b.setCanceledOnTouchOutside(true);
        b.show();
        btn_no = dialogView.findViewById(R.id.exit_No_btn);
        btn_yes = dialogView.findViewById(R.id.exit_Yes_btn);
        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                b.dismiss();
            }
        });
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    //for popup notification

//    public class BottomNavigationViewHelper {
//
//        FrameLayout badgeFrame;
//        TextView badgeView;
//
//        public void removeShiftMode(BottomNavigationView bottomNavigationView) {
//            BottomNavigationMenuView menuView = (BottomNavigationMenuView) bottomNavigationView.getChildAt(0);
//            View v = menuView.getChildAt(3);
//
//            BottomNavigationItemView itemView = (BottomNavigationItemView) v;
//
//            View badge = LayoutInflater.from(getApplicationContext()).inflate(R.layout.badge_bottom_bar, menuView, false);
//            badgeFrame = badge.findViewById(R.id.frame_badge);
//            badgeView = badge.findViewById(R.id.badge_number);
//            database.dbOpen();
//            int count = database.getItemsCount();
//            database.dbClose();
//
//            if (count == 0) {
//                badgeFrame.setVisibility(View.GONE);
//            } else {
//                badgeView.setText(String.valueOf(count));
//            }
//
//
//            itemView.addView(badge);
//
//
//        }
//
//
//    }


}


