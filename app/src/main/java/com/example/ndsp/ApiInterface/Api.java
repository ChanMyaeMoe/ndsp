package com.example.ndsp.ApiInterface;

import com.example.ndsp.Pojo.AuthorDetailResponse;
import com.example.ndsp.Pojo.AuthorListResponse;
import com.example.ndsp.Pojo.Blog;
import com.example.ndsp.Pojo.BlogResponse;
import com.example.ndsp.Pojo.Book;
import com.example.ndsp.Pojo.BookByCategoryResponse;
import com.example.ndsp.Pojo.Category;
import com.example.ndsp.Pojo.CategoryAll;
import com.example.ndsp.Pojo.CommentData;
import com.example.ndsp.Pojo.CommentResponse;
import com.example.ndsp.Pojo.EbookAllResponse;
import com.example.ndsp.Pojo.EbookDetailResponse;
import com.example.ndsp.Pojo.GenreDetailResponse;
import com.example.ndsp.Pojo.GenreListResponse;
import com.example.ndsp.Pojo.OrderResponse;
import com.example.ndsp.Pojo.PublisherDetailResponse;
import com.example.ndsp.Pojo.PublisherListResponse;
import com.example.ndsp.Pojo.RecentBookAllList;
import com.example.ndsp.Pojo.RecentBookDetailResponce;
import com.example.ndsp.Pojo.BookResponse;
import com.example.ndsp.Pojo.TenCategoryResponse;
import com.example.ndsp.Pojo.TopTenSeeAllBook;
import com.example.ndsp.model.Author;
import com.example.ndsp.model.Genre;
import com.example.ndsp.model.Publisher;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Streaming;
import retrofit2.http.Url;


public interface Api {


    @GET("/api/article/detail/{id}")
    Call<Blog> getArticleDetail(@Path("id") int id);

    @GET("/api/article/all")
    Call<BlogResponse> getArticleLists();

    @GET("/api/book/topten")
    Call<ArrayList<BookResponse>> getTopTenBook();

    @GET("/api/book/topall")
    Call<RecentBookAllList> getTopTenSeeAll(@Query("page") int page);

    @GET("/api/book/recent")
    Call<ArrayList<BookResponse>> getRecentbook();

    @GET("/api/book/all")
    Call<RecentBookAllList> getRecentBookAllList(@Query("page") int page);

    @GET("/api/book/detail/{book_id}")
    Call<RecentBookDetailResponce> getrecentbookdetail(@Path("book_id") String bookId);

    @GET("/api/category/show")
    Call<ArrayList<TenCategoryResponse>> getTenCategory();

    @GET("/api/category/all")
    Call<CategoryAll> getCategoryAll(@Query("page") int page);

    @GET("/api/book/by_category/{category_id}")
    Call<BookByCategoryResponse> getbooklistcategory(@Path("category_id") int categoryId,@Query("page") int page);

    @GET("/api/ebook/by_category/{category_id}")
    Call<BookByCategoryResponse> getEbookListByCategory(@Path("category_id") int categoryId, @Query("page") int page);

    @GET("api/author/all")
    Call<AuthorListResponse> getAuthorList(@Query("page") int page);


    @GET("api/publisher/all")
    Call<PublisherListResponse> getPublisherList(@Query("page") int page);


    @GET("api/genre/all")
    Call<GenreListResponse> getGenreList(@Query("page") int page);

    @GET("api/book/by_publisher/{publisher_id}")
    Call<PublisherDetailResponse> getPublisherDetail(@Path("publisher_id") int publisher_id,@Query("page") int page);

    @GET("api/book/by_author/{author_id}")
    Call<AuthorDetailResponse> getAuthorDetail(@Path("author_id") int author_id,@Query("page") int page);

    @GET("api/book/by_genre/{genre_id}")
    Call<GenreDetailResponse> getGenreDetail(@Path("genre_id") int genre_id,@Query("page") int page);

    @GET("/api/ebook/all")
    Call<EbookAllResponse> getEbookAll();

    @GET("/api/ebook/detail/{book_id}")
    Call<EbookDetailResponse> getEbookDetail(@Path("ebook_id") int ebook_id);

    @FormUrlEncoded
    @POST("/api/order")
    Call<OrderResponse> postOrder(@Field("order") JSONObject order);

    @FormUrlEncoded
    @POST("/api/comment/upload")
    Call<CommentResponse> postComment(@Field("comments") JSONObject comment);


    @GET("/api/comment/all/{book_id}")
    Call<CommentData> getComment(@Path("book_id") int book_id , @Query("page") int page);


    @GET("/api/category/search/{name}")
    Call<List<Category>> getSearchCategory(@Path("name") String name);

    @GET("/api/ebook/search/{name}")
    Call<List<Book>> getSearchEbook(@Path("name") String name);

    @GET("/api/publisher/search/{name}")
    Call<List<Publisher>> getSearchPublisher(@Path("name") String name);

    @GET("/api/author/search/{name}")
    Call<List<Author>> getSearchAuthor(@Path("name") String name);

    @GET("/api/book/search/{name}")
    Call<List<Book>> getSearchBook(@Path("name") String name);

    @GET("/api/genre/search/{name}")
    Call<List<Genre>> getSearchGenre(@Path("name") String name);

    @GET("/api/ebook/download/{name}")
    Call<ResponseBody> getPDF(@Path("name") String name);



    //for Searching

//    /api/category/search/{name}
//    /api/publisher/search/{name}
//    /api/author/search/{name}
//    /api/genre/search/{name}
//    /api/book/search/{name}//just a list
//    /api/search/book/{name}
//    /api/search/author/{name}
//    /api/search/category/{name}
//    /api/search/genre/{name}
//    /api/search/publisher/{name}


}
