package com.example.ndsp.Pojo;

public class User {

    private String email,address;
    private long phone;
    private int town_id;

    public User(){
        email=null;
        address=null;
        phone=0;
        town_id=0;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getPhone() {
        return phone;
    }

    public void setPhone(long phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setCity_id(int town_id){
        this.town_id=town_id;
    }
    public int getCity_id(){
        return town_id;
    }
}
