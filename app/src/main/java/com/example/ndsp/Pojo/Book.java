package com.example.ndsp.Pojo;

import com.google.gson.annotations.SerializedName;

public class Book {

    @SerializedName("id")
    public int id;

    @SerializedName("bookTitle")
    public String bookTitle;

}
