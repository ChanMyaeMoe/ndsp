package com.example.ndsp.Pojo;

import com.google.gson.annotations.SerializedName;

public class DataComment {

    @SerializedName("comment")
    public String comment;

    @SerializedName("rating")
    public String rating;

    @SerializedName("created_at")
    public String created_at;

    @SerializedName("username")
    public String username;

}
