package com.example.ndsp.Pojo;

import com.google.gson.annotations.SerializedName;

public class Category {

    @SerializedName("id")
    public int id;

    @SerializedName("categoryName")
    public String categoryName;

}
