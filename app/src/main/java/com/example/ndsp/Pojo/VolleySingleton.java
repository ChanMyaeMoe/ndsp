package com.example.ndsp.Pojo;


import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class VolleySingleton {

    private static Context context;
    private static VolleySingleton mSingleton;
    RequestQueue requestQueue;

    private VolleySingleton(Context paramContext) {

        context = paramContext;
        requestQueue = getRequestQueue();

    }

    public static VolleySingleton getInstance(Context paramContext) {

        try {
            if (mSingleton == null) {
                mSingleton = new VolleySingleton(paramContext);
            }

            return mSingleton;
        } finally {
        }
    }

    public <T> void addToRequestQueue(Request<T> paramRequest) {
        getRequestQueue().add(paramRequest);
    }

    public RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(context
                    .getApplicationContext());
        }
        return requestQueue;
    }
}
