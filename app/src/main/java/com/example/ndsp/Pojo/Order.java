package com.example.ndsp.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Order {

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("phone")
    @Expose
    public long phone;

    @SerializedName("town_id")
    @Expose
    public int town_id;

    @SerializedName("address")
    @Expose
    public String address;

    @SerializedName("books")
    @Expose
    public ArrayList<BookOrder> books=new ArrayList<>();
}
