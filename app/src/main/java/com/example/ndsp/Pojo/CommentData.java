package com.example.ndsp.Pojo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CommentData {

    @SerializedName("current_page")
    public int currentPage;

    @SerializedName("data")
    public List<DataComment> data;

    @SerializedName("last_page")
    public int last_page;

    @SerializedName("next_page_url")
    public String nextPageUrl;
}
