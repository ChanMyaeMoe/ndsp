package com.example.ndsp.Pojo;


import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class JsonConverter{

    private Context context;
    private User user;
    private ArrayList<OrderBook> orderBooks = new ArrayList<>();

    public JsonConverter(Context context, ArrayList<OrderBook> orderBooks) {
        this.context = context;
        this.orderBooks = orderBooks;

    }
    public JsonConverter(Context context){
        this.context = context;
    }

    public JSONObject convertJsonObj(User user){

        this.user = user;

        JSONObject orderObj;
        Map wholeMap = new HashMap();

        wholeMap.put("email",user.getEmail());
        wholeMap.put("phone",user.getPhone());
        wholeMap.put("address",user.getAddress());
        wholeMap.put("town_id",1);

        JSONArray orderedBooksArray = new JSONArray();

        for (int i=0;i<orderBooks.size();i++){

            Map<String ,String > bookMap = new HashMap<>();

            JSONObject bookObj;

            bookMap.put("book_id",orderBooks.get(i).getBook_id());
            bookMap.put("qty",String.valueOf(orderBooks.get(i).getBook_qty()));
            bookObj = new JSONObject(bookMap);

            orderedBooksArray.put(bookObj);
        }

//        wholeMap.put(LinkProvider.BOOKS,orderedBooksArray);


        orderObj = new JSONObject(wholeMap);

        return orderObj;
    }

    public JSONObject convertCommentJsonObject(String email,String comment,String rating,String bookId){

        JSONObject wholeJson;
        JSONObject object;

        Map map = new HashMap();
//        map.put(LinkProvider.BOOK_ID,bookId);
//        map.put(LinkProvider.NAME,email);
//        map.put(LinkProvider.COMMENT,comment);
//        map.put(LinkProvider.RATING,rating);

        object = new JSONObject(map);

        Map wholeMap = new HashMap();
//        wholeMap.put(LinkProvider.COMMENTS,object);
        wholeJson = new JSONObject(wholeMap);
        Log.i("comment_json",""+wholeJson);
        return object;
    }


}

