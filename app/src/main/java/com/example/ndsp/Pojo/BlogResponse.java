package com.example.ndsp.Pojo;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class BlogResponse {

    @SerializedName("data")
    public ArrayList<Blog> blogs;

}
