package com.example.ndsp.Pojo;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BookOrder {

    @SerializedName("book_id")
    @Expose
    public int book_id;

    @SerializedName("qty")
    @Expose
    public int qty;

}
