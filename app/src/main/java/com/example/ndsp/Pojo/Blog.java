package com.example.ndsp.Pojo;

import com.google.gson.annotations.SerializedName;

public class Blog {
    @SerializedName("id")
    public int blogId;

    @SerializedName("category")
    public String category;

    @SerializedName("title")
    public String title;

    @SerializedName("content")
    public String content;

    @SerializedName("created_at")
    public String createAt;

    @SerializedName("cover_photo")
    public String coverPhoto;

}
