package com.example.ndsp.Pojo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BookByCategoryResponse {

    @SerializedName("data")
    public List<BookByCategory> bookList;

}
