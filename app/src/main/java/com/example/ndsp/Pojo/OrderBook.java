package com.example.ndsp.Pojo;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class OrderBook {

    private String book_id, book_name;
    private int book_qty,book_price;

    public String getBook_id() {
        return book_id;
    }

    public void setBook_id(String book_id) {
        this.book_id = book_id;
    }

    public String getBook_name() {
        return book_name;
    }

    public void setBook_name(String book_name) {
        this.book_name = book_name;
    }

    public int getBook_qty() {
        return book_qty;
    }

    public void setBook_qty(int book_qty) {
        this.book_qty = book_qty;
    }

    public int getBook_price() {
        return book_price;
    }

    public void setBook_price(int  book_price) {
        this.book_price = book_price;
    }
}
