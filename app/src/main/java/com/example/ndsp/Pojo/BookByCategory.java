package com.example.ndsp.Pojo;

import com.google.gson.annotations.SerializedName;

public class BookByCategory {

    @SerializedName("id")
    public int id;

    @SerializedName("bookTitle")
    public String bookTitle;

    @SerializedName("bookCoverImgUrl")
    public String bookCoverImgUrl;

    @SerializedName("bookSalePrice")
    public int price;

    @SerializedName("category")
    public String category;

    @SerializedName("author")
    public String author;

    @SerializedName("rating")
    public int rating;

    @SerializedName("ebook_size")
    public String ebookSize;

    @SerializedName("e_book_download")
    public String ebookDownload;


}
