package com.example.ndsp.Interface;


import android.content.Context;
import android.util.Log;

import com.example.ndsp.Fragment.FragmentCart;
import com.example.ndsp.Pojo.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;


public class OrderPresenter implements DataRequester.ObjectRequestedListener {

    private Context context;
    private DataRequester requester;
    private FragmentCart fragment;

    public OrderPresenter(Context context,FragmentCart fragment) {
        this.context = context;
        this.fragment = fragment;
        requester = new DataRequester(context);
        requester.setObjListener(this);
    }

    public void makeOrder(JSONObject orderObj) {
        Log.i("orderSend",""+orderObj);

        requester.requestObjectPostOrder("http://128.199.217.182/api/order", VolleySingleton.getInstance(context).getRequestQueue(),orderObj);
    }
    @Override
    public void onObjectRequested(JSONObject obj) {

//        Log.i("orderUp",""+obj);
//        try {
////            fragment.requestedOrder(new SuccessParser().parseObj(obj));
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }

    }
}