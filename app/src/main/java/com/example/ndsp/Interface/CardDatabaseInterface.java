package com.example.ndsp.Interface;

public interface CardDatabaseInterface {

    public interface CartDataBaseInterface {
        void deleteItem (String name, int id);
        int editQty();
        void confirmClick();
    }

}
