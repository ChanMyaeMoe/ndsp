package com.example.ndsp.Interface;


import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by kyaw on 11/9/17.
 */

public class DataRequester {
    Context context;
    ArrayRequestedListener aryListener;
    ObjectRequestedListener objListener;
    ErrorResponseListener errorResponseListener;


    //constructor
    public DataRequester(Context context) {
        this.context = context;
    }


    public void setAryListener(ArrayRequestedListener listener) {
        aryListener = listener;
    }

    public void setObjListener(ObjectRequestedListener listener) {
        objListener = listener;
    }


    //get methods
    public void requestArray(final String url, RequestQueue queue) {

        JsonArrayRequest arrayRequest = new JsonArrayRequest(Request.Method.GET, url,

                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {

                        aryListener.onArrayRequested(response);
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
                        Log.d("Error",""+error);
                    }
                });

        arrayRequest.setRetryPolicy(new DefaultRetryPolicy(5000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(arrayRequest);

    }

    public void requestObject(final String url, RequestQueue queue) {


        JsonObjectRequest objectRequest = new JsonObjectRequest(Request.Method.GET, url, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                Log.i("ignore",""+response);
                objListener.onObjectRequested(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i("ignore_error",""+error+url);
            }
        });


        objectRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(objectRequest);

    }


    //post object requested
    public void requestedObjectPostComment(String url, RequestQueue queue, JSONObject object) {

        Map<String,String> map = new HashMap<>();
        map.put("comments",object.toString());
        JSONObject reqObj = new JSONObject(map);

        Log.i("comment_it",""+reqObj);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, reqObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {

                Log.i("comment_response",""+jsonObject);
                objListener.onObjectRequested(jsonObject);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {


                Log.i("comment_error",""+volleyError);
//                errorResponseListener.onErrorResponse(volleyError);
            }
        }
        ) {
        };

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(jsonObjectRequest);

    }


    public void requestObjectPost(String url, RequestQueue queue, JSONObject object) {

        Map<String ,JSONObject > map = new HashMap<>();
        map.put("order",object);
        JSONObject jsonRequest = new JSONObject(map);
        Log.i("htoke",""+jsonRequest);


        JSONObject strObj;
        Map<String,String> strMap = new HashMap<>();
        strMap.put("order",object.toString());
        strObj = new JSONObject(strMap);
        Log.i("loke",""+strObj);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, strObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {

                Log.i("response_it", "" + jsonObject);


                objListener.onObjectRequested(jsonObject);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError)

            {

                Log.i("error_it","reeor");
                //errorResponseListener.onErrorResponse();
            }
        }
        ) {

        };

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(jsonObjectRequest);

    }
    public void requestObjectPostOrder(String url, RequestQueue queue, JSONObject object) {

        Map<String ,JSONObject > map = new HashMap<>();
        map.put("order",object);
        JSONObject jsonRequest = new JSONObject(map);
        Log.i("htokeO",""+jsonRequest);


        JSONObject strObj;
        Map<String,String> strMap = new HashMap<>();
        strMap.put("order",object.toString());
        strObj = new JSONObject(strMap);
        Log.i("lokeO",""+strObj);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, strObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {

                Log.i("response_it", "" + jsonObject);


                objListener.onObjectRequested(jsonObject);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError)

            {

                Log.i("error_it","reeor");
                //errorResponseListener.onErrorResponse();
            }
        }
        ) {

        };

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(jsonObjectRequest);

    }




    public interface ArrayRequestedListener {

        void onArrayRequested(JSONArray ary);

    }

    public interface ObjectRequestedListener {

        void onObjectRequested(JSONObject obj);

    }

    public interface ErrorResponseListener {
        void onErrorResponse(VolleyError volleyError);
    }
}
