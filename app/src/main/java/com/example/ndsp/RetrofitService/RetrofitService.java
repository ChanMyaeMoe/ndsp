package com.example.ndsp.RetrofitService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class RetrofitService {
    public static final String BASE_URL="http://128.199.217.182";

//    public static final String BASE_URL="http://192.168.100.157:8000";
    public Retrofit getRetrofitService(){


        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit service=new Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                        .build();
                return service;
    }
}
